public class Ch02_Lab01{
	public static void main(String[] args){
		String name = "Ezra";
		int age = 22;
		double annualPay = 1000000;
		
		System.out.printf("My name is " + name + ", my age is " + age + " and I hope to earn $%,.2f per year.", annualPay);
	}
}