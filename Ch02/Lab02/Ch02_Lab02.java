public class Ch02_Lab02{
	public static void main(String[] args){
		String firstName = "Ezra";
		String middleName = "J.";
		String lastName = "Heard";
		
		char firstInitial = 'E';
		char middleInitial = 'J';
		char lastInitial = 'H';
		
		System.out.println("First Name: " + firstName);
		System.out.println("Middle Name: " + middleName);
		System.out.println("Last Name: " + lastName);
		System.out.println("First Initial: " + firstInitial);
		System.out.println("Middle Initial: " + middleInitial);
		System.out.println("Last Initial: " + lastInitial);
	}
}