import java.util.Scanner;

public class Ch02_Lab05{
	public static void main(String[] args){
		final int NUM_IN_BAG = 40;
		final int SERVINGS_IN_BAG = 10;
		final int CALORIES_PER_SERVING = 300;
		final int CALORIES_PER_COOKIE = CALORIES_PER_SERVING / (NUM_IN_BAG / SERVINGS_IN_BAG);
		
		int cookiesConsumed;
		int caloriesConsumed;
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.println("Enter the number of cookies you ate: ");
		
		cookiesConsumed = userInput.nextInt();
		
		caloriesConsumed = cookiesConsumed * CALORIES_PER_COOKIE;
		
		System.out.println("You consumed " + caloriesConsumed + " calories in cookies.");
	}
}