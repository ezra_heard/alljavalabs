public class Ch02_Lab06{
	public static void main(String[] args){
		double percentOnSales = 0.65;
		double totalSales = 8300000;
		
		double totalRev = percentOnSales * totalSales;
		
		System.out.printf("The East Coast division will generate: $%,.2f", totalRev);
	}
}