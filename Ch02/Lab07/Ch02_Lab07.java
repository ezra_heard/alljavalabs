public class Ch02_Lab07{
	public static void main(String[] args){
		final int ONE_ACRE = 43560;
		
		int landSize = 389767;
		double numOfAcres = (double)landSize / (double)ONE_ACRE;
		
		System.out.printf("There are %.2f acres in a tract of land of 389,767 square ft.", numOfAcres);
		
	}
}