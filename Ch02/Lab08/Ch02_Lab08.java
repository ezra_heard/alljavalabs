import java.util.Scanner;

public class Ch02_Lab08{
	public static void main(String[] args){
		final double STATE_TAX =  0.055;
		final double COUNTY_TAX = 0.02;
		
		double purchaseAmount;
		double stateTax;
		double countyTax;
		double totalTax;
		double totalAmount;
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.println("Enter purchase amount: ");
		purchaseAmount = userInput.nextDouble();
		
		stateTax = purchaseAmount * STATE_TAX;
		countyTax = purchaseAmount * COUNTY_TAX;
		totalTax = stateTax + countyTax;
		totalAmount = purchaseAmount + totalTax;
		
		System.out.printf("Purchase Price: $%,.2f\n", purchaseAmount);
		System.out.printf("State Tax: \t$%,.2f\n", stateTax);
		System.out.printf("County Tax: \t$%,.2f\n", countyTax);
		System.out.printf("Total Tax: \t$%,.2f\n", totalTax);
		System.out.printf("Total Price: \t$%,.2f\n", totalAmount);
	}
}