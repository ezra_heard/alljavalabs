import java.util.Scanner;

public class Ch02_Lab09{
	public static void main(String[] args){
		//MPG = Miles Driven / Gallons Consumed
		
		int milesDriven;
		int gallonsConsumed;
		double mpg;
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.println("How many miles did you drive:");
		milesDriven = userInput.nextInt();
		
		System.out.println("How many gallons of gas did you consume:");
		gallonsConsumed = userInput.nextInt();
		
		mpg = (double)milesDriven / (double)gallonsConsumed;
		
		System.out.printf("Your car's MPG is: %,.2f", mpg);
	}
}