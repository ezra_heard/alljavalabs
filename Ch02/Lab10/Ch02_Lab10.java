import java.util.Scanner;

public class Ch02_Lab10{
	public static void main(String[] args){
		double score1 = 0;
		double score2 = 0;
		double score3 = 0;
		double average;
		
		Scanner userInput = new Scanner(System.in);
		
		for(int i = 1; i <= 3; i++){
			System.out.println("Enter score " + i + ": ");
			
			switch (i){
				case 1:
					score1 = userInput.nextDouble();
					break;
				case 2:
					score2 = userInput.nextDouble();
					break;
				case 3:
					score3 = userInput.nextDouble();
					break;
			}
		}
		
		average = (score1 + score2 + score3) / 3;
		
		System.out.println("Score 1: " + score1 + "%");
		System.out.println("Score 2: " + score2 + "%");
		System.out.println("Score 3: " + score3 + "%");
		System.out.printf("Average: %.2f",average);
		System.out.println("%");
		
	}
}