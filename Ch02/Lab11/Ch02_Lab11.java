import java.util.Scanner;

public class Ch02_Lab11{
	public static void main(String[] args){
		int males;
		int females;
		int total;
		double percentMale;
		double percentFemale;
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.print("How many males are in the class: ");
		males = userInput.nextInt();
		
		System.out.print("How many females are in the class: ");
		females = userInput.nextInt();
		
		total = males + females;
		
		//Multiply by ten to display proper percentages
		percentMale = ((double)males / (double)total) * (double)100;
		percentFemale = ((double)females / (double)total) * (double)100;
		
		System.out.printf("Of the %d students, %.2f%c is male and %.2f%c is female.", total, percentMale, '%', percentFemale, '%');
	}
}