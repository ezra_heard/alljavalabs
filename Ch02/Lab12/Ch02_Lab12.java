import java.util.Scanner;

public class Ch02_Lab12{
	public static void main(String[] args){
		String city;
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.print("Enter your favorite city: ");
		city = userInput.nextLine();
		
		System.out.println("Number of character: " + city.length());
		System.out.println("In uppercase: " + city.toUpperCase());
		System.out.println("In lowercase: " + city.toLowerCase());
		System.out.println("First character: " + city.charAt(0));
	}
}