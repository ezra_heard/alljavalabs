import java.util.Scanner;

public class Ch02_Lab13{
	public static void main(String[] args){
		final double TAX_RATE = 0.075;
		final double TIP_RATE = 0.18;
		
		double mealPrice;
		double mealTax;
		double mealTip;
		double totalPrice;
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.print("Enter the meals price: $");
		mealPrice = userInput.nextDouble();
		
		mealTax = mealPrice * TAX_RATE;
		mealTip = mealPrice * TIP_RATE;
		totalPrice = mealPrice + mealTax + mealTip;
		
		System.out.printf("Price:\t$%.2f\n", mealPrice);
		System.out.printf("Tax:\t$%.2f\n", mealTax);
		System.out.printf("Tip:\t$%.2f\n", mealTip);
		System.out.printf("Total:\t$%.2f\n", totalPrice);
	}
}