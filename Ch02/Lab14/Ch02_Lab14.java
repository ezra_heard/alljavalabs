public class Ch02_Lab14{
	public static void main(String[] args){
		final int STOCKS_BOUGHT = 1000;
		final double PRICE_PER_SHARE = 25.5;
		final double COMMISSION = 0.02;
		
		double priceOfStocks;
		double priceOfCommission;
		double totalPrice;
		
		priceOfStocks = STOCKS_BOUGHT * PRICE_PER_SHARE;
		priceOfCommission = priceOfStocks * COMMISSION;
		totalPrice = priceOfStocks + priceOfCommission;
		
		System.out.printf("Stocks Price:     $%.2f\n", priceOfStocks);
		System.out.printf("Commission Price: $%.2f\n", priceOfCommission);
		System.out.printf("Total Price:      $%.2f\n", totalPrice);
	}
}