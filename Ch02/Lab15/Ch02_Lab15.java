import java.util.Scanner;

public class Ch02_Lab15{
	public static void main(String[] args){
		final double CUPS_SUGAR_48 = 1.5;
		final double CUPS_BUTTER_48 = 1.0;
		final double CUPS_FLOUR_48 = 2.75;
		
		double cupsSugarPerCookie = CUPS_SUGAR_48 / 48;
		double cupsButterPerCookie = CUPS_BUTTER_48 / 48;
		double cupsFlourPerCookie = CUPS_FLOUR_48 / 48;
		
		int cookiesWanted;
		double sugarNeeded;
		double butterNeeded;
		double flourNeeded;
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.print("How many cookies do you want to make: ");
		cookiesWanted = userInput.nextInt();
		
		sugarNeeded = cookiesWanted * cupsSugarPerCookie;
		butterNeeded = cookiesWanted * cupsButterPerCookie;
		flourNeeded = cookiesWanted * cupsFlourPerCookie;
		
		System.out.printf("Sugar Cups: %.2f\n", sugarNeeded);
		System.out.printf("Butter Cups: %.2f\n", butterNeeded);
		System.out.printf("Flour Cups: %.2f\n", flourNeeded);
	}
}