public class Ch02_Lab16{
	public static void main(String[] args){
		final int TOTAL_CUSTOMERS = 15000;
		final double PERCENT_BOUGHT = 0.18;
		final double PERCENT_CITRUS = 0.58;
		
		double numBought;
		double numCitrus;
		
		numBought = TOTAL_CUSTOMERS * PERCENT_BOUGHT;
		numCitrus = numBought * PERCENT_CITRUS;
		
		System.out.printf("Number of customers who bought drinks: %.0f\n", numBought);
		System.out.printf("Of those who bought drinks %.0f prefer citrus", numCitrus);
	}
}