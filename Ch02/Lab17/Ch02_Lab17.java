import java.util.Scanner;

public class Ch02_Lab17{
	public static void main(String[] args){
		
		//Declare Variables
		String name;
		String age;
		String city;
		String college;
		String profession;
		String petSpecies;
		String petName;
		
		Scanner userInput = new Scanner(System.in);
		
		//Ask user for input
		System.out.print("Enter your name: ");
		name = userInput.nextLine();
		
		System.out.print("Enter your age: ");
		age = userInput.nextLine();
		
		System.out.print("What city do you live in? ");
		city = userInput.nextLine();
		
		System.out.print("Where did you attend college? ");
		college = userInput.nextLine();
		
		System.out.print("What is your profession? ");
		profession = userInput.nextLine();
		
		System.out.print("What species of pet do you own? ");
		petSpecies = userInput.nextLine();
		
		System.out.print("Enter your pet's name: ");
		petName = userInput.nextLine();
		
		//Print results
		System.out.printf("There once was a person named %s who lived in %s.\nAt the age of %s, %s went to college at %s.\n%s graduated and went to work as a %s.\nThen, %s adopted a(n) %s named %s.\nThey both lived happily ever after!", name, city, age, name, college, name, profession, name, petSpecies, petName);
	}
}