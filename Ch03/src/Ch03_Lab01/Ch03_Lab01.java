package Ch03_Lab01;

public class Ch03_Lab01 {

	public static void main(String[] args) {
		Employee employee1 = new Employee();
		Employee employee2 = new Employee();
		Employee employee3 = new Employee();
		
		employee1.setName("Susan Meyers");
		employee1.setIdNumber(47899);
		employee1.setDepartment("Accounting");
		employee1.setPosition("Vice President");
		
		employee2.setName("Mark Jones");
		employee2.setIdNumber(39119);
		employee2.setDepartment("IT");
		employee2.setPosition("Programmer");
		
		employee3.setName("Joy Rogers");
		employee3.setIdNumber(81774);
		employee3.setDepartment("Manufacturing");
		employee3.setPosition("Engineer");
		
		System.out.println("Name: " + employee1.getName());
		System.out.println("Id Number: " + employee1.getIdNumber());
		System.out.println("Department: " + employee1.getDepartment());
		System.out.println("Position: " + employee1.getPosition() + "\n");
		
		System.out.println("Name: " + employee2.getName());
		System.out.println("Id Number: " + employee2.getIdNumber());
		System.out.println("Department: " + employee2.getDepartment());
		System.out.println("Position: " + employee2.getPosition() + "\n");
		
		System.out.println("Name: " + employee3.getName());
		System.out.println("Id Number: " + employee3.getIdNumber());
		System.out.println("Department: " + employee3.getDepartment());
		System.out.println("Position: " + employee3.getPosition());
	}

}
