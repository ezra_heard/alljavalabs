package Ch03_Lab01;

public class Employee {
	private String name;
	private int idNumber;
	private String department;
	private String position;
	
	public void setName(String name) { this.name = name; }
	public String getName() {return this.name; }
	
	public void setIdNumber(int idNumber) { this.idNumber = idNumber; }
	public int getIdNumber() {return this.idNumber; }

	public void setDepartment(String department) { this.department = department; }
	public String getDepartment() {return this.department; }
	
	public void setPosition(String position) { this.position = position; }
	public String getPosition() {return this.position; }
}
