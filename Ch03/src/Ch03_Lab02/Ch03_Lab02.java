package Ch03_Lab02;

public class Ch03_Lab02 {

	public static void main(String[] args) {
		Car car1 = new Car(2018, "Honda");
		
		car1.accelerate();
		System.out.println(car1.getSpeed());
		car1.accelerate();
		System.out.println(car1.getSpeed());
		car1.accelerate();
		System.out.println(car1.getSpeed());
		car1.accelerate();
		System.out.println(car1.getSpeed());
		car1.accelerate();
		System.out.println(car1.getSpeed());
		
		car1.brake();
		System.out.println(car1.getSpeed());
		car1.brake();
		System.out.println(car1.getSpeed());
		car1.brake();
		System.out.println(car1.getSpeed());
		car1.brake();
		System.out.println(car1.getSpeed());
		car1.brake();
		System.out.println(car1.getSpeed());
	}

}
