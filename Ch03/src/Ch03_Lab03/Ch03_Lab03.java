package Ch03_Lab03;

public class Ch03_Lab03 {

	public static void main(String[] args) {
		PersonalInformation info1 = new PersonalInformation();
		PersonalInformation info2 = new PersonalInformation();
		PersonalInformation info3 = new PersonalInformation();
		
		info1.setName("Ezra");
		info1.setAddress("123 Test Lane");
		info1.setAge(22);
		info1.setPhoneNumber("123-456-7890");
		
		info2.setName("Nick");
		info2.setAddress("456 Psudo Street");
		info2.setAge(21);
		info2.setPhoneNumber("123-456-7890");
		
		info3.setName("Logan");
		info3.setAddress("789 Instance Field");
		info3.setAge(20);
		info3.setPhoneNumber("123-456-7890");
	}

}
