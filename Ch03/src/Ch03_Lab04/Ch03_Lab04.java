package Ch03_Lab04;
import java.util.Scanner;

public class Ch03_Lab04 {

	public static void main(String[] args) {
		Temperture temp;
		double userTemp;
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.print("Enter a Tempature: ");
		userTemp = userInput.nextDouble();
		
		temp = new Temperture(userTemp);
		
		System.out.printf("%.1f\n", temp.getFehrenheit());
		System.out.printf("%.1f\n", temp.getCelsius());
		System.out.printf("%.1f\n", temp.getKelvin());
		
		userInput.close();
	}
}
