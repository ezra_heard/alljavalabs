package Ch03_Lab04;

public class Temperture {
	private double ftemp;
	
	public Temperture(double temp) {
		this.ftemp = temp;
	}
	
	public void setFahrenheit(double temp) { this.ftemp = temp; }
	public double getFehrenheit() {return ftemp;}
	
	public double getCelsius() {
		return (((double)5/9) * (ftemp - 32));
	}
	
	public double getKelvin() {
		return (((double)5/9) * (ftemp - 32)) + 273;
	}
}
