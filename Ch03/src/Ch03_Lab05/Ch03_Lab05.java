package Ch03_Lab05;

public class Ch03_Lab05 {

	public static void main(String[] args) {
		RetailItem item1 = new RetailItem();
		RetailItem item2 = new RetailItem();
		RetailItem item3 = new RetailItem();
		
		item1.setDescription("Jacket");
		item1.setUnitsOnHand(12);
		item1.setPrice(59.95);
		
		item2.setDescription("Designer Jeans");
		item2.setUnitsOnHand(40);
		item2.setPrice(34.95);
		
		item3.setDescription("Shirt");
		item3.setUnitsOnHand(20);
		item3.setPrice(24.95);
	}

}
