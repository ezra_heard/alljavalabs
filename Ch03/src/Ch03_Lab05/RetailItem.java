package Ch03_Lab05;

public class RetailItem {
	private String description;
	private int unitsOnHand;
	private double price;
	
	public void setDescription(String description) {this.description = description; }
	public String getDescription() { return description; }
	
	public void setUnitsOnHand(int units) {this.unitsOnHand = units; }
	public int getUnitsOnHand() { return unitsOnHand; }
	
	public void setPrice(double price) {this.price = price; }
	public double getPrice() { return price; }
	
}
