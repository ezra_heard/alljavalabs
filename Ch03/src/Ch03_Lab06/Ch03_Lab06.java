package Ch03_Lab06;

import java.util.Scanner;

public class Ch03_Lab06 {

	public static void main(String[] args) {
		Payroll employee1 = new Payroll("Ezra", "9999999");
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.print("Enter the employees hourly pay: ");
		employee1.setHourlyPay(userInput.nextDouble());
		
		System.out.print("Enter the employees hours worked: ");
		employee1.setHoursWorked(userInput.nextDouble());
		
		System.out.printf("The employees gross pay is: $%,.2f", employee1.calcGrossPay());
		
		userInput.close();
	}

}
