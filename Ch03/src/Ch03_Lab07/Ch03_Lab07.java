package Ch03_Lab07;

public class Ch03_Lab07 {

	public static void main(String[] args) {
		WidgetPlant factory = new WidgetPlant();
		
		int numberOfWidgets = 300;
		
		factory.setNumberOfWidgets(numberOfWidgets);
		
		System.out.printf("It will take %.2f days to make %d widgets", factory.CalcDaysToMake(), numberOfWidgets);
	}

}
