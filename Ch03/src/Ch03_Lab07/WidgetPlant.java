package Ch03_Lab07;

public class WidgetPlant {
	private final int WIDGETS_PER_HOUR = 10;
	private final int SHIFTS = 2;
	private final int HOURS_PER_SHIFT = 8;
	private final int WIDGETS_PER_DAY = (HOURS_PER_SHIFT * SHIFTS) * WIDGETS_PER_HOUR;
	
	private int numberOfWidgets;
	
	public void setNumberOfWidgets(int widgets) { numberOfWidgets = widgets; }
	
	public double CalcDaysToMake() {
		return (double)numberOfWidgets / WIDGETS_PER_DAY;
	}
}
