package Ch03_Lab08;
import java.util.Scanner;

public class Ch03_Lab08 {

	public static void main(String[] args) {
		TestScores scoreSet = new TestScores();

		Scanner userInput = new Scanner(System.in);
		
		System.out.print("Enter score 1: ");
		scoreSet.setScore1(userInput.nextDouble());
		
		System.out.print("Enter score 2: ");
		scoreSet.setScore2(userInput.nextDouble());
		
		System.out.print("Enter score 3: ");
		scoreSet.setScore3(userInput.nextDouble());
		
		System.out.printf("The average score is: %.2f", scoreSet.calcAverageScore());
	
		userInput.close();
	}

}
