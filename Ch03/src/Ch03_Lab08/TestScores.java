package Ch03_Lab08;

public class TestScores {
	private double score1;
	private double score2;
	private double score3;
	
	public void setScore1(double score) { score1 = score; }
	public double getScore1() { return score1; }
	
	public void setScore2(double score) { score2 = score; }
	public double getScore2() { return score3; }
	
	public void setScore3(double score) { score3 = score; }
	public double getScore3() { return score3; }
	
	public double calcAverageScore() {
		return (score1 + score2 + score3) / 3;
	}
}
