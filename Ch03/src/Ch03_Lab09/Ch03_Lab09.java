package Ch03_Lab09;

import java.util.Scanner;

public class Ch03_Lab09 {

	public static void main(String[] args) {
		double radius = 0;
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.print("What is your circle's radius? ");
		radius = userInput.nextDouble();
		
		Circle circle = new Circle(radius);
		
		System.out.printf("For a circle with a radius of %.2f\n", circle.getRadius());
		System.out.printf("\tThe area is: %.2f\n", circle.getArea());
		System.out.printf("\tThe diameter is: %.2f\n", circle.getDiameter());
		System.out.printf("\tThe circumference is: %.2f\n", circle.getCircumference());
		
		userInput.close();
	}

}
