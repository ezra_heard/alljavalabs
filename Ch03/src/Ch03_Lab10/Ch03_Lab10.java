package Ch03_Lab10;

import java.util.Scanner;

public class Ch03_Lab10 {

	public static void main(String[] args) {
		Pet userPet = new Pet();
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.print("What is the pet's name? ");
		userPet.setName(userInput.nextLine());
		
		System.out.print("What species of pet is it? ");
		userPet.setType(userInput.nextLine());
		
		System.out.print("How old is the pet? ");
		userPet.setAge(userInput.nextInt());
		
		System.out.println("Pet Info\n\tName: " + userPet.getName() + "\n\tType: " + userPet.getType() + "\n\tAge: " + userPet.getAge());
	
		userInput.close();
	}

}
