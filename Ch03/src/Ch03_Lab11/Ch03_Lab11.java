package Ch03_Lab11;

public class Ch03_Lab11 {

	public static void main(String[] args) {
		Patient patient1 = new Patient("Lucetti", "Michael", "Josefa", "1313 Park Street", "Whiteston", "11378", "123-456-7890", "James Strode", "987-654-3210");
		Procedure procedure1 = new Procedure("Physical Exam", "1/22/2018", "Dr. Irvine", 250.00);
		Procedure procedure2 = new Procedure("X-Ray", "1/22/2018", "Dr. Jamison", 500.00);
		Procedure procedure3 = new Procedure("Blood Test", "1/22/2018", "Dr. Smith", 200.00);
	
		//Print Patient info
		System.out.println("Patient Info\n\tName: " + patient1.getFirstName() + " " + patient1.getMiddleName() + " " + patient1.getLastName() + "\n\tAddress: " + patient1.getAddress() + ", " + patient1.getCity() + " " + patient1.getZipCode() + "\n\tPhone Number: " + patient1.getPhoneNumber() + "\n\nEmergency Contact Info\n\tName: " + patient1.getEmergencyName() + "\n\tPhone Number: " + patient1.getEmergencyPhone());
		
		
		System.out.println();
		
		
		//Print Procedures
		System.out.printf("Procedure 1\n\tProcedure Name: " + procedure1.getProcedureName() + "\n\tDate: " + procedure1.getDate() + "\n\tPracctitioner: " + procedure1.getPractitioner() + "\n\tCharge: $%.2f", procedure1.getCharge());
		System.out.println();
		System.out.println();
		System.out.printf("Procedure 2\n\tProcedure Name: " + procedure2.getProcedureName() + "\n\tDate: " + procedure2.getDate() + "\n\tPracctitioner: " + procedure2.getPractitioner() + "\n\tCharge: $%.2f", procedure2.getCharge());
		System.out.println();
		System.out.println();
		System.out.printf("Procedure 3\n\tProcedure Name: " + procedure3.getProcedureName() + "\n\tDate: " + procedure3.getDate() + "\n\tPracctitioner: " + procedure3.getPractitioner() + "\n\tCharge: $%.2f", procedure3.getCharge());
		
		
		System.out.println();
		System.out.println();
		
		
		//Print Total Charge
		System.out.printf("Total Charge for procedures: %,.2f", (procedure1.getCharge() + procedure2.getCharge() + procedure3.getCharge()));
		
	}

}
