package Ch03_Lab11;

public class Patient {
	private String firstName;
	private String middleName;
	private String lastName;
	private String address;
	private String city;
	private String zipCode;
	private String phoneNumber;
	private String emergencyName;
	private String emergencyPhone;
	
	public Patient (String firstName, String middleName, String lastName, String address, String city, String zipCode, String phoneNumber, String emergencyName, String emergencyPhone) {
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.address = address;
		this.city = city;
		this.zipCode = zipCode;
		this.phoneNumber = phoneNumber;
		this.emergencyName = emergencyName;
		this.emergencyPhone = emergencyPhone;
	}
	
	public void setFirstName(String firstName) { this.firstName = firstName;}
	public String getFirstName() {return this.firstName; }
	
	public void setMiddleName(String middleName) { this.middleName = middleName;}
	public String getMiddleName() {return this.middleName; }
	
	public void setLastName(String lastName) { this.lastName = lastName;}
	public String getLastName() {return this.lastName; }
	
	public void setAddress(String address) { this.address = address;}
	public String getAddress() {return this.address; }
	
	public void setCity(String city) { this.city = city;}
	public String getCity() {return this.city; }
	
	public void setZipCode(String zipCode) { this.zipCode = zipCode;}
	public String getZipCode() {return this.zipCode; }
	
	public void setPhoneNumber(String phoneNumber) { this.phoneNumber = phoneNumber;}
	public String getPhoneNumber() {return this.phoneNumber; }
	
	public void setEmergencyName(String emergencyName) { this.emergencyName = emergencyName;}
	public String getEmergencyName() {return this.emergencyName; }
	
	public void setEmergencyPhone(String emergencyPhone) { this.emergencyPhone = emergencyPhone;}
	public String getEmergencyPhone() {return this.emergencyPhone; }
}
