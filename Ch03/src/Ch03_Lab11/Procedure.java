package Ch03_Lab11;

public class Procedure {
	private String procedureName;
	private String date;
	private String practitioner;
	private double charge;
	
	public Procedure(String procedureName, String date, String practitioner, double charge) {
		this.procedureName = procedureName;
		this.date = date;
		this.practitioner = practitioner;
		this.charge = charge;
	}
	
	public void setProcedureName(String procedureName) { this.procedureName = procedureName; }
	public String getProcedureName() { return this.procedureName; }
	
	public void setDate(String date) { this.date = date; }
	public String getDate() { return this.date; }
	
	public void setPractitioner(String practitioner) { this.practitioner = practitioner; }
	public String getPractitioner() { return this.practitioner; }
	
	public void setCharge(double charge) { this.charge = charge; }
	public double getCharge() { return this.charge; }
}
