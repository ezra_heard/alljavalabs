package Ch04_Lab02;

import java.util.Scanner;

public class Ch04_Lab02 {
	final static int SECONDS_IN_MINUTE = 60;
	final static int SECONDS_IN_HOUR = 3600;
	final static int SECONDS_IN_DAY = 86400;

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		int seconds = 0;
		
		System.out.print("Enter number of seconds: ");
		seconds = userInput.nextInt();
		
		if(seconds < 60) {
			System.out.println("You enter too few seconds");
		}
		
		while(seconds >= SECONDS_IN_MINUTE) {
			if(seconds >= SECONDS_IN_DAY) {
				int days = calcDays(seconds);
				seconds -= days * SECONDS_IN_DAY;
				
				System.out.print("Days: " + days  + "\n");
			}
			else if(seconds >= SECONDS_IN_HOUR) {
				int hours = calcHours(seconds);
				seconds -= hours * SECONDS_IN_HOUR;
				
				System.out.print("Hours: " + hours  + "\n");
			}
			else if(seconds >= SECONDS_IN_MINUTE) {
				int minutes = calcMinutes(seconds);
				seconds -= minutes * SECONDS_IN_MINUTE;
				
				System.out.print("Minutes: " + minutes  + "\n");
			}
			
			if(seconds < 60)
				System.out.print("Seconds: " + seconds);
		}
		
		userInput.close();
	}
	
	private static int calcMinutes(int seconds) {
		//There are 60 seconds in an minute
		return seconds / SECONDS_IN_MINUTE;
	}
	
	private static int calcHours(int seconds) {
		//There are 3,600 seconds in an hour
		return seconds / SECONDS_IN_HOUR;
	}
	
	private static int calcDays(int seconds) {
		//There are 86,400 seconds in an day
		return seconds / SECONDS_IN_DAY;
	}

}
