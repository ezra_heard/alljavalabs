package Ch04_Lab03;

public class TestScores {
	private int score1;
	private int score2;
	private int score3;
	
	
	public TestScores(int score1, int score2, int score3) {
		this.score1 = score1;
		this.score2 = score2;
		this.score3 = score3;
	}
	
	public int getScore1() { return score1; }
	public int getScore2() { return score2; }
	public int getScore3() { return score3; }
	
	public double calcAverageScore() {
		return ((double)score1 + score2 + score3) / 3;
	}
	
	public String getLetterGrade() {
		double average = this.calcAverageScore();
		
		if(average >= 90)
			return "A";
		else if(average >= 80)
			return "B";
		else if(average >= 70)
			return "C";
		else if(average >= 60)
			return "D";
		else
			return "F";
	}
}
