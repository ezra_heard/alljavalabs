package Ch04_Lab04;

public class SoftwareSales {
	private final double PACKAGE_PRICE = 99.00;
	
	private int units;
	
	public SoftwareSales(int unitsSold) {
		units = unitsSold;
	}
	
	public double calcPurcahseCost() {
		double basePrice = units * PACKAGE_PRICE;
		
		if(units >= 10 && units <= 19)
			return basePrice * 0.80; // 20% Discount
		
		else if (units >= 20 && units <= 49)
			return basePrice * 0.70; // 30% Discount
		
		else if (units >= 50 && units <= 99)
			return basePrice * 0.60; // 40% Discount
		
		else if (units >= 100)
			return basePrice * 0.50; // 50% Discount
		
		else
			return basePrice; // No Discount
	}
	
}
