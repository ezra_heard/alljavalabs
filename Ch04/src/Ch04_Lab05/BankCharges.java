package Ch04_Lab05;

public class BankCharges {
	private int checksWriten;
	private double balance;
	
	public BankCharges(int checksWriten, double balance) {
		this.checksWriten = checksWriten;
		this.balance = balance;
	}
	
	public double calcMonthlyServiceFee() {
		//Base fee of $10 per month
		double fee = 10.00;
		
		//Calculate Check fee
		if(checksWriten <= 19)
			fee += (checksWriten * 0.10);
		else if(checksWriten >= 20 && checksWriten <= 39)
			fee += (checksWriten * 0.08);
		else if(checksWriten >= 40 && checksWriten <= 59)
			fee += (checksWriten * 0.06);
		else if(checksWriten >= 60)
			fee += (checksWriten * 0.04);
		
		//If balance is under $400 (before checking fees are applied)
		//Bank charges another $15
		if(balance < 400) {
			fee += 15;
		}
			
		//Calculate ending balance
		balance -= fee;
		
		return fee;
	}
}
