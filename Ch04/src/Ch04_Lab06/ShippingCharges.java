package Ch04_Lab06;

public class ShippingCharges {
	private double weight;
	
	public ShippingCharges(double weight) {
		this.weight = weight;
	}
	
	public double calcCharge(int miles) {
		if(weight < 2)
			return 1.10 * (int)(miles / 500);
		else if(weight >= 2 && weight < 6)
			return 2.20 * (int)(miles / 500);
		else if(weight >= 6 && weight < 10)
			return 3.70 * (int)(miles / 500);
		else
			return 4.80 * (int)(miles / 500);
	}
}
