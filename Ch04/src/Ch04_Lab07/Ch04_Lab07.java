package Ch04_Lab07;

import java.util.Scanner;

public class Ch04_Lab07 {

	public static void main(String[] args) {
		int calories, fat;
		double percentFromFat;
		FatGram fatGram = new FatGram();;
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.print("Enter number of calories: ");
		calories = userInput.nextInt();
		
		System.out.print("Enter amount of fat: ");
		fat = userInput.nextInt();
		
		fatGram.setCaloriesAndFat(calories, fat);
		
		percentFromFat = fatGram.calcPercentFromFat();
		
		if(percentFromFat > 1)
			System.out.println("!!Invlaid Entries!!\nCan't have more calories from fat than there are in the food");
		else {
			System.out.printf("%.2f%% of this food's calories come from fat", (percentFromFat * 100));
			
			if(percentFromFat < 0.30)
				System.out.println("\nThis food is low in fat!");
		}
		
		userInput.close();
	}

}
