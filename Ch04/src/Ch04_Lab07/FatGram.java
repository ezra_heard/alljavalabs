package Ch04_Lab07;

public class FatGram {
	private int calories;
	private int fat;
	
	public void setCaloriesAndFat(int calories, int fat) {
		this.calories = calories;
		this.fat = fat;
	}
	
	public double calcPercentFromFat() {
		return ((double)fat * 9) / calories;
	}
}