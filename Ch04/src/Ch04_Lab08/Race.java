package Ch04_Lab08;

public class Race {
	private String runner1Name, runner2Name, runner3Name;
	private int runner1Time, runner2Time, runner3Time;
	
	public Race(String name1, String name2, String name3, int time1, int time2, int time3) {
		runner1Name = name1;
		runner2Name = name2;
		runner3Name = name3;
		runner1Time = time1;
		runner2Time = time2;
		runner3Time = time3;
	}
	
	public String get1stPlace() {
		if(runner1Time < runner2Time && runner1Time < runner3Time)
			return runner1Name;
		else if(runner2Time < runner3Time)
			return runner2Name;
		else
			return runner3Name;
	}
	
	public String get2ndPlace() {
		if((runner1Time < runner2Time && runner1Time > runner3Time) || (runner1Time < runner3Time && runner1Time > runner2Time))
			return runner1Name;
		else if((runner2Time < runner1Time && runner2Time > runner3Time) || (runner2Time < runner3Time && runner2Time > runner1Time))
			return runner2Name;
		else
			return runner3Name;
	}
	
	public String get3rdPlace() {
		if(runner1Time > runner2Time && runner1Time > runner3Time)
			return runner1Name;
		else if(runner2Time > runner3Time)
			return runner2Name;
		else
			return runner3Name;
	}
}
