package Ch04_Lab09;

import java.util.Scanner;

public class Ch04_Lab09 {

	public static void main(String[] args) {
		String userChoice;
		
		SpeedOfSound speedOfSound = new SpeedOfSound();
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.print("What material is sound traveling through?\n(Air/Water/Steel): ");
		userChoice = userInput.nextLine();
		
		System.out.print("In feet, how far is the sound traveling? ");
		speedOfSound.setDistance(userInput.nextInt());
		
		if(userChoice.equalsIgnoreCase("air"))
			System.out.printf("It will take %f seconds to travel %dft through %s", speedOfSound.getSpeedInAir(), speedOfSound.getDistance(), userChoice );
		else if(userChoice.equalsIgnoreCase("water"))
			System.out.printf("It will take %f seconds to travel %dft through %s", speedOfSound.getSpeedInWater(), speedOfSound.getDistance(), userChoice );
		else if(userChoice.equalsIgnoreCase("steel"))
			System.out.printf("It will take %f seconds to travel %dft through %s", speedOfSound.getSpeedInSteel(), speedOfSound.getDistance(), userChoice );
		else
			System.out.println("Error! You did not pick a valid material for sound to travel through!");
	
		userInput.close();
	}

}
