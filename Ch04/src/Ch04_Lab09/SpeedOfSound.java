package Ch04_Lab09;

public class SpeedOfSound {
	private int distance;
	
	public void setDistance(int distance) { this.distance = distance; }
	public int getDistance() { return this.distance; };
	
	public double getSpeedInAir() {
		return (double)distance/1100;
	}
	
	public double getSpeedInWater() {
		return (double)distance/4900;
	}
	
	public double getSpeedInSteel() {
		return (double)distance/16400;
	}
}
