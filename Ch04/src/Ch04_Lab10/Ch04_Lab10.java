package Ch04_Lab10;

import java.util.Scanner;

public class Ch04_Lab10 {

	public static void main(String[] args) {
		FreezeOrBoil freezeOrBoil = new FreezeOrBoil();
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.print("Enter a temperature: ");
		freezeOrBoil.setTemperature(userInput.nextInt());
		
		System.out.printf("At %d:\n", freezeOrBoil.getTemperature());
		
		if(freezeOrBoil.isEthylFreezzing())
			System.out.print("Ethyl will freeze\n");
		
		if(freezeOrBoil.isEthylBoiling())
			System.out.print("Ethyl will boil\n");
		
		if(freezeOrBoil.isOxygenFreezing())
			System.out.print("Oxygen will freeze\n");
		
		if(freezeOrBoil.isOxygenBoiling())
			System.out.print("Oxygen will boil\n");
		
		if(freezeOrBoil.isWaterFreezing())
			System.out.print("Water will freeze\n");
		
		if(freezeOrBoil.isWaterBoiling())
			System.out.print("Water will boil\n");
		
		userInput.close();
	}
}
