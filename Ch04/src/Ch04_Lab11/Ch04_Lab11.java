package Ch04_Lab11;

import java.util.Scanner;

public class Ch04_Lab11 {

	public static void main(String[] args) {
		String mobilePackage;
		int minutes;
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.print("Enter your package letter\n(A/B/C): ");
		mobilePackage = userInput.nextLine();
		
		if(mobilePackage.equalsIgnoreCase("a") || mobilePackage.equalsIgnoreCase("b") || mobilePackage.equalsIgnoreCase("c")){
			System.out.print("How many minutes did you use? ");
			minutes = userInput.nextInt();
			
			MobileCharges bill = new MobileCharges(mobilePackage, minutes);
			
			System.out.printf("You monthly charge is $%,.2f", bill.calcMonthlyCharge());
		}
		else 
			System.out.printf("Invalid entry, package %s does not exsist", mobilePackage);
	
		userInput.close();
	}

}
