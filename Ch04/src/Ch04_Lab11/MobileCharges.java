package Ch04_Lab11;

public class MobileCharges {
	private String mobilePackage;
	private int mobileMinutes;
	
	public MobileCharges(String mobilePackage, int minutes) {
		this.mobilePackage = mobilePackage;
		this.mobileMinutes = minutes;
	}
	
	public double calcMonthlyCharge() {
		if(mobilePackage.equalsIgnoreCase("a")) {
			if(mobileMinutes > 450) 
				return 39.99 + (0.45 * (mobileMinutes - 450));
			else 
				return 39.99;
		}
		else if(mobilePackage.equalsIgnoreCase("b")) {
			if(mobileMinutes > 900) 
				return 59.99 + (0.40 * (mobileMinutes - 900));
			else 
				return 59.99;
		}
		else if(mobilePackage.equalsIgnoreCase("c"))
				return 69.99;
		else
			return -1;
	}
}
