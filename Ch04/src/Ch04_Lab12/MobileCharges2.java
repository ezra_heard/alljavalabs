package Ch04_Lab12;

public class MobileCharges2 {
	private String mobilePackage2;
	private int mobileMinutes2;
	
	public MobileCharges2(String mobilePackage, int minutes) {
		this.mobilePackage2 = mobilePackage;
		this.mobileMinutes2 = minutes;
	}
	
	public double calcMonthlyCharge() {
		if(mobilePackage2.equalsIgnoreCase("a")) {
			if(mobileMinutes2 > 450) 
				return 39.99 + (0.45 * (mobileMinutes2 - 450));
			else 
				return 39.99;
		}
		else if(mobilePackage2.equalsIgnoreCase("b")) {
			if(mobileMinutes2 > 900) 
				return 59.99 + (0.40 * (mobileMinutes2 - 900));
			else 
				return 59.99;
		}
		else if(mobilePackage2.equalsIgnoreCase("c"))
				return 69.99;
		else
			return -1;
	}
	
	public String testPossibleSavings() {
		if(mobilePackage2.equalsIgnoreCase("a")) {
			double chargeA;
			double chargeB;
			double chargeC = 69.99;
			
			String savings = "";
			
			if(mobileMinutes2 > 450)
				chargeA = 39.99 + (0.45 * (mobileMinutes2 - 450));
			else 
				chargeA = 39.99;
			
			if(mobileMinutes2 > 900) 
				chargeB =  59.99 + (0.40 * (mobileMinutes2 - 900));
			else 
				chargeB =  59.99;
			
			if(chargeA > chargeB)
				savings += String.format("You'd save $%,.2f by switching to package B", (chargeA - chargeB));
			
			if(chargeA > chargeC) {
				if(!savings.equalsIgnoreCase(""))
					savings += "\n";
				
				savings += String.format("You'd save $%,.2f by switching to package C", (chargeA - chargeC));
			
			}
			
			return savings;
		}
		
		else if(mobilePackage2.equalsIgnoreCase("b")) {
			double chargeB;
			double chargeC = 69.99;
			
			if(mobileMinutes2 > 900) 
				chargeB =  59.99 + (0.40 * (mobileMinutes2 - 900));
			else 
				chargeB =  59.99;
			
			if(chargeB > chargeC)
				return String.format("You'd save $%,.2f by switching to package C", (chargeB - chargeC));
		}
		
		return "no savings";
			
	}
}
