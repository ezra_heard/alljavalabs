package Ch04_Lab13;

import java.util.Scanner;

public class Ch04_Lab13 {

	public static void main(String[] args) {
		int height;
		double weight;
		double bmi;
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.print("Enter height in inches: ");
		height = userInput.nextInt();
		
		System.out.print("Enter weight in pounds: ");
		weight = userInput.nextDouble();
		
		bmi = weight * (703 / Math.pow(height, 2));
		
		if(bmi >= 18.5 && bmi <= 25)
			System.out.println("This person has an optimal weight");
		else if(bmi < 18.5)
			System.out.println("This person is underweight");
		else if(bmi > 25)
			System.out.println("This person is overweight");
		
		userInput.close();
	}

}
