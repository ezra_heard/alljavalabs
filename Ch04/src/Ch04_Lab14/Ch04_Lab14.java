package Ch04_Lab14;

import java.util.Scanner;

public class Ch04_Lab14 {

	public static void main(String[] args) {
		int month, year;
		MonthDays monthCalendar;
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.print("Enter a month: ");
		month = userInput.nextInt();
		
		System.out.print("Enter a year: ");
		year = userInput.nextInt();
		
		monthCalendar = new MonthDays(month, year);
		
		System.out.println("" + monthCalendar.getNumberOfDays() + " days");
		
		userInput.close();
	}

}
