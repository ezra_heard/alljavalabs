package Ch04_Lab14;

public class MonthDays {
	private int month;
	private int year;
	
	public MonthDays(int month, int year) {
		this.month = month;
		this.year = year;
	}
	
	public int getNumberOfDays() {
		boolean leapYear = false;
		
		if((year % 100 == 0 && year % 400 == 0) || (year % 100 != 0 && year % 4 == 0))
			leapYear = true;
		
		if(month == 1 || month == 3 || month == 5 || month == 7 || month ==  8 || month == 10 || month == 12)
			return 31;
		else if(month == 4 || month == 6 || month == 9 || month ==  11)
			return 30;
		else if(!leapYear)
			return 28;
		else
			return 29;
	}
}
