package Ch04_Lab15;

import java.util.Scanner;

public class Ch04_Lab15 {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		
		System.out.print("How many books did you purchase this month? ");
		calcPoints(userInput.nextInt());
		
		userInput.close();
	}

	
	private static void calcPoints(int books) {
		if(books == 0)
			System.out.println("This month, you earned 0 points for purchasing 0 books");
		else if(books == 1)
			System.out.println("This month, you earned 5 points for purchasing 1 books");
		else if(books == 2)
			System.out.println("This month, you earned 15 points for purchasing 2 books");
		else if(books == 3)
			System.out.println("This month, you earned 30 points for purchasing 3 books");
		else if(books >= 4)
			System.out.println("This month, you earned 60 points for purchasing " + books + " books");
		else
			System.out.println("You can't buy negative books");
	}
}
