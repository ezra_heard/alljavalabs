package Ch04_Lab16;

import java.util.Scanner;

public class Ch04_Lab16 {

	public static void main(String[] args) {
		int day, month, year;
		
		MagicDate date;
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.print("Enter day (dd/mm/yy): ");
		day = userInput.nextInt();
		
		System.out.print("Enter month (" + day + "/mm/yy): ");
		month = userInput.nextInt();
		
		System.out.print("Enter year (" + day + "/" + month + "/yy): ");
		year = userInput.nextInt();
		
		////Finish here with the rest of the code
		date = new MagicDate(day, month, year);
		
		System.out.print("The date " + day + "/" + month + "/" + year + " is ");
		
		if(date.isMagic())
			System.out.println("a magic date");
		else
			System.out.println("not a magic date");
		
		userInput.close();
	}

}
