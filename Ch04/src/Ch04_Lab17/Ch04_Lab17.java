package Ch04_Lab17;

import java.util.Scanner;

public class Ch04_Lab17 {

	public static void main(String[] args) {
		final int HOTDOGS_IN_PACKAGE = 10;
		final int BUNS_IN_PACKAGE = 8;
		
		int peopleAttending, hotdogsReceived, totalHotdogs, hotdogPacksNeeded, bunPacksNeeded, remaingHotdogs, remainingBuns;
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.print("How many people are attending? ");
		peopleAttending = userInput.nextInt();

		System.out.print("How may hot dogs does each person receive? ");
		hotdogsReceived = userInput.nextInt();
		
		totalHotdogs = peopleAttending * hotdogsReceived;
		
		if(totalHotdogs % HOTDOGS_IN_PACKAGE == 0) {
			hotdogPacksNeeded = totalHotdogs / HOTDOGS_IN_PACKAGE;
			remaingHotdogs = 0;
		}
		else {
			hotdogPacksNeeded = totalHotdogs / HOTDOGS_IN_PACKAGE + 1;
			remaingHotdogs = (hotdogPacksNeeded * HOTDOGS_IN_PACKAGE) % totalHotdogs;
		}
		
		if(totalHotdogs % BUNS_IN_PACKAGE == 0) {
			bunPacksNeeded = totalHotdogs / BUNS_IN_PACKAGE;
			remainingBuns = 0;
		}
		else {
			bunPacksNeeded = totalHotdogs / BUNS_IN_PACKAGE + 1;
			remainingBuns = (bunPacksNeeded * BUNS_IN_PACKAGE) % totalHotdogs;
		}
		
		System.out.println("Packs of Hotdogs needed: " + hotdogPacksNeeded);
		System.out.println("Leftover Hotdogs: " + remaingHotdogs);
		System.out.println("Packs of Buns needed: " + bunPacksNeeded);
		System.out.println("Leftover Buns: " + remainingBuns);
		
		userInput.close();

		
	}

}
