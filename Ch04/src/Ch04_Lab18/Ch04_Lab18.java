package Ch04_Lab18;

import java.util.Scanner;

public class Ch04_Lab18 {

	public static void main(String[] args) {
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.print("Enter a pocket number: ");
		
		RoulettePocket pocket = new RoulettePocket(userInput.nextInt());
		
		System.out.println("That pocket is: " + pocket.getPocketColor());
		
		userInput.close();
		
	}

}
