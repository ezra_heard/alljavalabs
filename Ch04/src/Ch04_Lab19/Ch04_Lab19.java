package Ch04_Lab19;

import java.util.Scanner;

public class Ch04_Lab19 {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		
		System.out.println("Reboot the computer and try to connect.");
		checkWithUser();
		if(userInput.nextLine().equalsIgnoreCase("n")) {
			System.out.println("\nReboot the router and try to connect.");
			checkWithUser();
			if(userInput.nextLine().equalsIgnoreCase("n")) {
				System.out.println("\nMake sure the cables between the router & modem are plugged in firmly.");
				checkWithUser();
				if(userInput.nextLine().equalsIgnoreCase("n")) {
					System.out.println("\nMove the router to a new location.");
					checkWithUser();
					if(userInput.nextLine().equalsIgnoreCase("n")) {
						System.out.println("\nGet a new router.");
					}
				}
			}
			
		}
		
		userInput.close();
		
	}

	private static void checkWithUser() {
		System.out.print("Did that fix the problem? (y/n) ");
	}
}
