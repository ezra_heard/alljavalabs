package Ch05_Lab01;

import java.util.Scanner;

public class Ch05_Lab01 {

	public static void main(String[] args) {
		int num, total = 0;
		
		Scanner userInput = new Scanner(System.in);
	
		System.out.print("Enter a number: ");
		
		num = userInput.nextInt();
		
		for(int i = 1; i <= num; ++i) {
			total += i;
		}
		
		System.out.println("Total: " + total);
		
		userInput.close();
	}

}
