package Ch05_Lab02;

import java.util.Scanner;

public class Ch05_Lab02 {

	public static void main(String[] args) {
		Vehicle transport = new Vehicle();
		
		Scanner userInput = new Scanner(System.in);

		do {
			
			System.out.print("Enter travel time (hours): ");
			transport.setHours(userInput.nextInt());
			
			if(transport.getHours() < 1)
				System.out.println("Invalid time");
			
		} while(transport.getHours() < 1);
		
		//Pre-initialize the value to invalid
		transport.setMph(-1);
		
		do {
			
			System.out.print("Enter travel speed (mph): ");
			transport.setMph(userInput.nextInt());
			
			if(transport.getMph() < 0)
				System.out.println("Invalid speed");
		
		} while(transport.getMph() < 0);
		
		int hours = transport.getHours();
		
		for(int i = 1; i <= hours; i++) {
			System.out.printf("Hour: %02d | Distance: %d miles\n", i, (transport.getDistance() - (transport.getMph() * (hours - i))));
		}
		
		userInput.close();
	}

}
