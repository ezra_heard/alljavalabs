package Ch05_Lab02;

public class Vehicle {
	private int hours;
	private int mph;
	
	public int getHours() { return hours; }
	public void setHours(int hours) { this.hours = hours; }
	
	public int getMph() { return mph; }
	public void setMph(int mph) { this.mph = mph; }
	
	public int getDistance() {
		return hours * mph;
	}
}
