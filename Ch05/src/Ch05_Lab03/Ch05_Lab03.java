package Ch05_Lab03;

import java.io.*;
import java.util.Scanner;
import Ch05_Lab02.Vehicle;

public class Ch05_Lab03 {

	public static void main(String[] args) throws IOException{
		Vehicle transport = new Vehicle();
		
		Scanner userInput = new Scanner(System.in);

		do {
			
			System.out.print("Enter travel time (hours): ");
			transport.setHours(userInput.nextInt());
			
			if(transport.getHours() < 1)
				System.out.println("Invalid time");
			
		} while(transport.getHours() < 1);
		
		//Pre-initialize the value to invalid
		transport.setMph(-1);
		
		do {
			
			System.out.print("Enter travel speed (mph): ");
			transport.setMph(userInput.nextInt());
			
			if(transport.getMph() < 0)
				System.out.println("Invalid speed");
		
		} while(transport.getMph() < 0);
		
		userInput.close();
		
		int hours = transport.getHours();
		
		PrintWriter pw = new PrintWriter("src\\Ch05_Lab03\\TravelInfo.txt");
		
		for(int i = 1; i <= hours; i++) {
			pw.printf("Hour: %02d | Distance: %d mile", i, (transport.getDistance() - (transport.getMph() * (hours - i))));
			pw.println();
		}
		
		pw.close();
	}
}
