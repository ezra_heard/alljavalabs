package Ch05_Lab04;

import java.util.Scanner;

public class Ch05_Lab04 {

	public static void main(String[] args) {
		int days = 0;
		double pay = 0.01;
		
		Scanner userInput = new Scanner(System.in);
		
		for( ; days < 1; ) {
			System.out.print("Enter a number of days: ");
			days = userInput.nextInt();
		}
		
		for(int i = 2; i <= days; ++i) {
			pay *= 2;
		}
		
		System.out.printf("Your pay after %d days will be $%,.2f", days, pay);
	
		userInput.close();
	}
}
