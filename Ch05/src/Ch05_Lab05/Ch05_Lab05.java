package Ch05_Lab05;

import java.util.Scanner;

public class Ch05_Lab05 {

	public static void main(String[] args) {
		int floors = 0, totalRooms = 0, totalOccupied = 0;
		
		Scanner userInput = new Scanner(System.in);

		for(; floors < 1;) {
			System.out.print("Enter the number of floors: ");
			floors = userInput.nextInt();
			
			if(floors < 1)
				System.out.println("Invalid number of floors: can't be less than 1.");
		}
		
		for(int i = 1; i <= floors; i++) {
			int rooms = -2, occupied = -1;
			
			for(; rooms < occupied;) {
				for(; rooms < 10; ) {
					System.out.print("Enter rooms on floor " + i + ": ");
					rooms = userInput.nextInt();
					
					if(rooms < 10)
						System.out.println("Invlaid number of rooms: can't be less than 10");
				}
				
				for(; occupied < 0; ) {
					System.out.print("Enter rooms occupied on floor " + i + ": ");
					occupied = userInput.nextInt();
					
					if(occupied < 0)
						System.out.println("Invlaid number of occupied rooms: can't be less than 0");
					}
				
				if(occupied > rooms) {
					System.out.println("Invlaid numbers: you can't have more rooms occupied then exist on the floor.");
					rooms = -2;
					occupied = -1;
				}
			}
			
			totalRooms += rooms;
			totalOccupied += occupied;
			
		}
		userInput.close();
		
		System.out.println("This hotel has: ");
		System.out.println("\tTotal Rooms: " + totalRooms);
		System.out.println("\tOccupied Rooms: " + totalOccupied);
		System.out.println("\tVacant Rooms: " + (totalRooms - totalOccupied));
		System.out.printf("\nThe occupancy rate is: %.0f%%", (((double)totalOccupied / totalRooms) * 100));
		
	}

}
