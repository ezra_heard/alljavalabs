package Ch05_Lab06;

import java.util.Scanner;

public class Ch05_Lab06 {

	public static void main(String[] args) {
		PopulationGrowth pop = new PopulationGrowth();
		Scanner userInput = new Scanner(System.in);
		
		for(; pop.getOrganisms() < 2;) {
			System.out.print("Enter population size: ");
			pop.setOrganisms(userInput.nextDouble());
			
			if(pop.getOrganisms() < 2)
				System.out.println("Invaild number for population: cannot be less than 2");
		}
		
		for(pop.setDailyGrowth(-1); pop.getDailyGrowth() < 0;) {
			System.out.print("Enter population growth rate (percentage): ");
			pop.setDailyGrowth((userInput.nextDouble() / 100));
			
			if(pop.getDailyGrowth() < 0)
				System.out.println("Invaild number for growth rate: cannot be less than 0");
		}
		
		for(; pop.getGrowthPeriod() < 1;) {
			System.out.print("Enter days of growth: ");
			pop.setGrowthPeriod(userInput.nextInt());
			
			if(pop.getGrowthPeriod() < 1)
				System.out.println("Invaild number for growth period: cannot be less than 1");
		}
		
		pop.displayGrowth();
		
		userInput.close();
		
	}

}
