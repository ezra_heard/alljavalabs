package Ch05_Lab06;

public class PopulationGrowth {
	double organisms;
	double dailyGrowth;
	int growthPeriod;
	
	public double getOrganisms() {
		return organisms;
	}
	public void setOrganisms(double organisms) {
		this.organisms = organisms;
	}
	public double getDailyGrowth() {
		return dailyGrowth;
	}
	public void setDailyGrowth(double dailyGrowth) {
		this.dailyGrowth = dailyGrowth;
	}
	public int getGrowthPeriod() {
		return growthPeriod;
	}
	public void setGrowthPeriod(int growthPeriod) {
		this.growthPeriod = growthPeriod;
	}
	
	public void displayGrowth() {
		double dailyOrganisms = this.organisms;
		
		for(int i = 0; i <= growthPeriod; ++i) {
			if(i != 0)
				dailyOrganisms += (dailyOrganisms * dailyGrowth);
			
			System.out.printf("Day %02d | Population: %.2f \n", i, dailyOrganisms);
		}
	}
}
