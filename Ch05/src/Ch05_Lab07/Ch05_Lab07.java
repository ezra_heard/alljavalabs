package Ch05_Lab07;

import java.util.Scanner;

public class Ch05_Lab07 {

	public static void main(String[] args) {
		int years = 0;
		int totalRainfall= 0;
		
		Scanner userInput = new Scanner(System.in);
		
		for(; years < 1 ;) {
			System.out.print("Enter number of years: ");
			years = userInput.nextInt();
			
			if(years < 1)
				System.out.println("Invalid number of year: can't be less than 1");			
		}
		
		for(int i = 1; i <= years; ++i) {
			for(int x = 1; x <= 12; ++x) {
				int rainfall = -1;
				
				for(; rainfall < 0 ;) {
					System.out.printf("Enter Year %d Month %d's Ranfall: ", i, x);
					rainfall = userInput.nextInt();
					
					if(rainfall < 0)
						System.out.println("Invalid number: rainfall can't be negative");
				}
				
				totalRainfall += rainfall;
			}
		}
		
		System.out.println("After " + years + " years of rainfall: ");
		System.out.println("\t Months Passed: " + (years * 12));
		System.out.printf("\t Total Rainfall: %d inches\n", totalRainfall);
		System.out.printf("\t Average Monthly Rainfall: %.2f/in", (totalRainfall / (double)(years * 12)));
		
		userInput.close();
	}

}
