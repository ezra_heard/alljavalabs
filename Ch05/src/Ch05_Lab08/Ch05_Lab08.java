package Ch05_Lab08;

import java.util.Scanner;

public class Ch05_Lab08 {

	public static void main(String[] args) {
		int lowest = Integer.MAX_VALUE;
		int highest = Integer.MIN_VALUE;
		
		Scanner userInput = new Scanner(System.in);
		
		for(int num = 0; num != -99; ) {
			System.out.print("Enter a number (-99 to end): ");
			num = userInput.nextInt();
			
			if(num != -99) {
				if(num < lowest)
					lowest = num;
				
				if(num > highest)
					highest = num;
			}
		}
		
		if(lowest == Integer.MAX_VALUE) {
			lowest = 0;
			highest = 0;
		}
		
		System.out.println("Lowest Number: " + lowest);
		System.out.println("Highest Number: " + highest);
		
		userInput.close();
	}

}
