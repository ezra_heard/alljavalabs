package Ch05_Lab09;

import java.util.Scanner;

public class Ch05_Lab09 {

	public static void main(String[] args) {
		String output = "";
		boolean sentinal = true;
		
		Scanner userInput = new Scanner(System.in);
		
		do {
			String id;
			
			System.out.print("Enter the employee ID (0 to exit): ");
			id = userInput.nextLine();
			
			if(id.equals("0"))
				sentinal = false;
			
			if(sentinal) {
				double gross = 0, state = 0, fed = 0, fica = 0;
				
				do{
					gross = chcekNeg("Enter the employees gross pay: ", userInput);
					
					do {
						state = chcekNeg("Enter the employees state tax: ", userInput);
						
						if(state > gross)
							System.out.println("Invalid input: can't have more state tax than gross pay");
					}while(state > gross);
					
					do {
						fed = chcekNeg("Enter the employees federal tax: ", userInput);
						
						if(state > gross)
							System.out.println("Invalid input: can't have more state tax than gross pay");
					}while(fed > gross);
					
					do {
						fica = chcekNeg("Enter the employees FICA withholdings: ", userInput);
						
						if(state > gross)
							System.out.println("Invalid input: can't have more state tax than gross pay");
					}while(fica > gross);
							
					if(gross <= (state + fed + fica))
						System.out.println("Invalid inputs: can't have less gross pay than fees removed\nPlease re-enter values");
					
				}while(gross <= (state + fed + fica));
				
				Payroll payroll = new Payroll(id, gross, state, fed, fica);
				
				output += String.format("Employee ID: %s\n\tGross Pay: $%,.2f\n\tState Tax: $%,.2f\n\tFederal Tax: $%,.2f\n\tFICA Withholdings: $%,.2f\n\tNet Pay: $%,.2f\n", payroll.getEmployeeID(), payroll.getGrossPay(), payroll.getStateTax(), payroll.getFederalTax(), payroll.getFicaWithholdings(), payroll.getNetPay());
			
				//Clear keyboard buffer
				userInput.nextLine();
			}
			
		}while(sentinal);
		
		if(!output.equals(""))
			System.out.println(output);
		
		userInput.close();	
	}
	
	private static double chcekNeg(String output, Scanner userInput) {
		double num = 0;
		
		do {
			System.out.print(output);
			num = userInput.nextDouble();
		}while(num < 0);
		
		return num;
	}

}
