package Ch05_Lab09;

public class Payroll {
	private String employeeID;
	private double grossPay;
	private double stateTax;
	private double federalTax;
	private double ficaWithholdings;
	
	public Payroll(String employeeID, double grossPay, double stateTax, double federalTax, double ficaWithholdings) {
		super();
		this.employeeID = employeeID;
		this.grossPay = grossPay;
		this.stateTax = stateTax;
		this.federalTax = federalTax;
		this.ficaWithholdings = ficaWithholdings;
	}

	public String getEmployeeID() {
		return employeeID;
	}

	public double getGrossPay() {
		return grossPay;
	}

	public double getStateTax() {
		return stateTax;
	}

	public double getFederalTax() {
		return federalTax;
	}

	public double getFicaWithholdings() {
		return ficaWithholdings;
	}
	
	public double getNetPay() {
		return grossPay - stateTax - federalTax - ficaWithholdings;
	}
}
