package Ch05_Lab10;

import java.util.Scanner;

public class Ch05_Lab10 {

	public static void main(String[] args) {
		double annualInterest, balance, deposits = 0, withdrawals = 0, totalInterest = 0;
		int months;
		
		SavingsAccount account;
		
		Scanner userInput = new Scanner(System.in);
	
		System.out.print("Enter annual interest rate: ");
		annualInterest = userInput.nextDouble();
		
		System.out.print("Enter starting balance: ");
		balance = userInput.nextDouble();
		
		account = new SavingsAccount(annualInterest, balance);
		
		System.out.print("Enter number of months: ");
		months = userInput.nextInt();
		
		for(int i = 1; i <= months; ++i) {
			double deposit, withdrawal;
			
			System.out.println("Month " + i);
			
			//Get Months Deposit
			System.out.print("Enter amount deposited: ");
			deposit = userInput.nextDouble();
			
			account.deposit(deposit);
			deposits += deposit;
			
			//Get Months Withdrawal
			System.out.print("Enter amount withdrawn: ");
			withdrawal = userInput.nextDouble();
			
			account.withdrawal(withdrawal);
			withdrawals += withdrawal;
			
			//Accrue month's interest
			totalInterest += account.accrueInterest();
		}
		
		System.out.printf("\nTotal of deposits: $%.2f", deposits);
		System.out.printf("\nTotal of withdrawals: $%.2f", withdrawals);
		System.out.printf("\nTotal interest accrued: $%.2f", totalInterest);
		System.out.printf("\nFinal Balance: $%.2f", account.getBalance());
		//Total interest = currentBalance - (balance + deposits - withdrawals)
		
		userInput.close();
	}

}
