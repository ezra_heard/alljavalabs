package Ch05_Lab10;

public class SavingsAccount {
	private double annualInterestRate;
	private double balance;
	
	public SavingsAccount(double annualInterestRate, double balance) {
		this.annualInterestRate = annualInterestRate;
		this.balance = balance;
	}
	
	public double getBalance() {
		return balance;
	}
	
	public boolean withdrawal(double withdrawalAmount) {
		if(balance - withdrawalAmount < 0)
			return false;
		else
			balance -= withdrawalAmount;
		
		return true;
	}
	
	public void deposit(double depositAmount) {
		balance += depositAmount;
	}
	
	public double accrueInterest() {
		double interestRate = annualInterestRate / 12;
		double interest = (balance * interestRate);
		
		balance += interest;
		
		return interest;
	}
}
