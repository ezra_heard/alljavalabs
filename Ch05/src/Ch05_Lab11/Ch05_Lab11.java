package Ch05_Lab11;

import java.util.Scanner;
import java.io.*;
import Ch05_Lab10.SavingsAccount;

public class Ch05_Lab11 {

	public static void main(String[] args) throws IOException{
		SavingsAccount account = new SavingsAccount(0.01, 500.00);
		
		File depositFile = new File("src/Ch05_Lab11/Deposits.txt");
		File withdrawalFile = new File("src/Ch05_Lab11/Withdrawals.txt");
		
		Scanner deposits = new Scanner(depositFile);
		Scanner withdrawls = new Scanner(withdrawalFile);
		
		while(deposits.hasNext()) {
			account.deposit(deposits.nextDouble());
		}
		
		while(withdrawls.hasNext()) {
			account.withdrawal(withdrawls.nextDouble());
		}
		
		System.out.printf("Total Interest Earned: $%.2f\n", account.accrueInterest());
		System.out.printf("Ending Balance: $%.2f", account.getBalance());
		
		deposits.close();
		withdrawls.close();
	}

}
