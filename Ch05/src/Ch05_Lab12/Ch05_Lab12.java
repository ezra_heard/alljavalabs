package Ch05_Lab12;

import java.util.Scanner;

public class Ch05_Lab12 {

	public static void main(String[] args) {
		int[] stores = new int[5];
		
		Scanner userInput = new Scanner(System.in);
		
		for(int i = 1; i <= 5; ++i) {
			System.out.print("Enter today's sales for store " + i + ": ");
			stores[(i-1)] = userInput.nextInt();
		}
		
		System.out.println("SALES BAR CHART");
		
		for(int i = 0; i < 5; ++i) {
			System.out.print("Store " + (i + 1) + ": ");
			
			for(int x = 1; x <= (stores[i] / 100); ++x) {
				System.out.print("*");
			}
			
			System.out.println();
		}
		
		userInput.close();
	}

}
