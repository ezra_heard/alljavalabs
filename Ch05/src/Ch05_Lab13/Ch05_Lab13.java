package Ch05_Lab13;

public class Ch05_Lab13 {

	public static void main(String[] args) {
		
		for(int i = 0; i <= 20; ++i) {
			System.out.printf("Centigrade: %02d | Fahrenheit: %.1f\n", i, convertToFehrenheit(i));
		}
	}

	private static double convertToFehrenheit(double temp) {
		return (((double)9 / 5) * temp) + 32;
	}
}
