package Ch05_Lab14;

import java.io.*;

public class Ch05_Lab14 {

	public static void main(String[] args) throws IOException {
		FileDisplay fd = new FileDisplay("src/Ch05_Lab14/Don_Quixote(Ch01).txt");
	
		//Feel free to uncomment which ever methods you like...
		//...they all display correctly its just a lot of text if you run all of them at once
		
		//fd.displayHead();
		//fd.displayContents();
		fd.displayWithLineNumbers();
	}

}
