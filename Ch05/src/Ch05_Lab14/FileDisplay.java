package Ch05_Lab14;

import java.util.Scanner;
import java.io.*;

public class FileDisplay {
	private String fileName;
	
	public FileDisplay(String fileName) {
		this.fileName = fileName;
	}
	
	public void displayHead() throws IOException {
		File file = new File(fileName);
		Scanner inputFile = new Scanner(file);
		
		for(int i = 1; i <= 5; ++i) {
			if(inputFile.hasNext())
				System.out.println(inputFile.nextLine());
		}
		
		inputFile.close();
	}
	
	public void displayContents() throws IOException {
		File file = new File(fileName);
		Scanner inputFile = new Scanner(file);
		
		for(;inputFile.hasNext();)
			System.out.println(inputFile.nextLine());
				
		inputFile.close();
	}
	
	public void displayWithLineNumbers() throws IOException {
		File file = new File(fileName);
		Scanner inputFile = new Scanner(file);
		
		for(int i = 1; inputFile.hasNext(); ++i)
			System.out.println(i + ": " + inputFile.nextLine());
		
		inputFile.close();
	}
}
