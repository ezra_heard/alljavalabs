package Ch05_Lab15;

import java.util.Scanner;
import java.io.*;

public class UpperCaseFile {	
	public UpperCaseFile(String fileName1, String fileName2) throws IOException{
		File file = new File(fileName1);
		Scanner inputFile = new Scanner(file);
		
		PrintWriter outputFile = new PrintWriter(fileName2);
		
		for(;inputFile.hasNext();) {
			outputFile.println(inputFile.nextLine().toUpperCase());
		}
		
		inputFile.close();
		outputFile.close();
	}
}
