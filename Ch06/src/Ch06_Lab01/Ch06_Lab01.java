package Ch06_Lab01;

public class Ch06_Lab01 {

	public static void main(String[] args) {
		System.out.printf("Area of a Circle: %.2f\n", Area.calcArea(50));
		System.out.printf("Area of a Rectangle: %.2f\n", Area.calcArea(20, 40));
		System.out.printf("Area of a Cylinder: %.2f", Area.calcArea(50, 40, Math.PI));
	}
}
