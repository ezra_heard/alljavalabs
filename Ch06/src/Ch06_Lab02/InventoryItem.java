package Ch06_Lab02;

public class InventoryItem {
	private String description;
	private int units;
	
	public InventoryItem() {
		description = "";
		units = 0;
	}
	
	public InventoryItem(String d) {
		description = d;
		units = 0;
	}
	
	public InventoryItem(String d, int u) {
		description = d;
		units = u;
	}
	
	public InventoryItem(InventoryItem _object) {
		this.description = _object.description;
		this.units = _object.units;
	}

	public void setDescription(String d) {
		this.description = d;
	}

	public void setUnits(int u) {
		this.units = u;
	}
	
	public String getDescription() { return description; }
	
	public int getUnits() { return units; }
}
