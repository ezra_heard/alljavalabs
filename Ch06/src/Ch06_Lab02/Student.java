package Ch06_Lab02;

public class Student {

	private String name;
	private int id;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Student()
	{
	
	}
	
	public Student(Student object2)
	{
		this.name = object2.name;
		this.id = object2.id;
	}
	
	
}
