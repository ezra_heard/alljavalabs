package Ch06_Lab02;

public class StudentDriver {

	public static void main(String[] args) {

		Student s1 = new Student();
		s1.setId(12345);
		s1.setName("Bill");
		
		Student s2 = new Student(s1);
		
		System.out.println(s1.getId() + " " + s1.getName());
		

		System.out.println(s2.getId() + " " + s2.getName());
	}

}
