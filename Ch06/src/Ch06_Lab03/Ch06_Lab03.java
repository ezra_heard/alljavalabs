package Ch06_Lab03;

import java.util.Scanner;

public class Ch06_Lab03 {

	public static void main(String[] args) {
		//Declare Variables
		Scanner userInput = new Scanner(System.in);
		double length, width, cost;
		RoomDimension room;
		RoomCarpet carpet;
		
		//Get User Input for length
		System.out.print("Enter Room Length: ");
		length = userInput.nextDouble();
		
		//Get User Input for width
		System.out.print("Enter Room Width: ");
		width = userInput.nextDouble();
		
		//Generate Room
		room = new RoomDimension(length, width);
		
		//Get User Input for cost
		System.out.print("Enter carpet cost (per square foot): ");
		cost = userInput.nextDouble();
		
		//Generate Carpet
		carpet = new RoomCarpet(room, cost);
		
		//Display Results
		System.out.printf("Your carpet will cost: $%.2f", carpet.getTotalCost());
		
		//Clean up
		userInput.close();
	}

}
