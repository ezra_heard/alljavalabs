package Ch06_Lab04;

import java.util.Scanner;

public class Ch06_Lab04 {

	public static void main(String[] args) {
		LandTract tract1 = null, tract2 = null;
		Scanner userInput = new Scanner(System.in);
		
		for(int i = 1; i <= 2; i++) {
			double length, width;
			
			System.out.print("Enter length for Tract " + i + ": ");
			length = userInput.nextDouble();
			
			System.out.print("Enter width for Tract " + i + ": ");
			width = userInput.nextDouble();
			
			switch(i) {
				case 1:
					tract1 = new LandTract(length, width);
				case 2:
					tract2 = new LandTract(length, width);
			}
		}
		
		if(tract1.equals(tract2))
			System.out.println("Tract 1 and Tract 2 are of equal size");
		else
			System.out.println("Tract 1 and Tract 2 are different sizes");
		
		userInput.close();
	}

}
