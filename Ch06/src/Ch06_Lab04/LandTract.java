package Ch06_Lab04;

public class LandTract {
	private double length;
	private double width;
	
	public LandTract(double l, double w) {
		length = l;
		width = w;
	}
	
	public double getArea(){
		return length * width;
	}
	
	public boolean equals(LandTract tract) {
		if(this.getArea() == tract.getArea())
			return true;
		else
			return false;
	}
	
	@Override
	public String toString() {
		return "Length: " + length + "\nWidth: " + width;
	}
}
