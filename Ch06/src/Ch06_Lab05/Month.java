package Ch06_Lab05;

public class Month {
	private int monthNumber;
	
	public Month() {
		monthNumber = 1;
	}
	
	public Month(int month) {
		if(month < 1 || month > 12)
			monthNumber = 1;
		else
			monthNumber = month;
	}
	
	public Month(String monthName) {
		switch(monthName.toLowerCase()) {
		case "january":
			monthNumber = 1;
			break;
		case "february":
			monthNumber = 2;
			break;
		case "march":
			monthNumber = 3;
			break;
		case "april":
			monthNumber = 4;
			break;
		case "may":
			monthNumber = 5;
			break;
		case "june":
			monthNumber = 6;
			break;
		case "july":
			monthNumber = 7;
			break;
		case "august":
			monthNumber = 8;
			break;
		case "september":
			monthNumber = 9;
			break;
		case "october":
			monthNumber = 10;
			break;
		case "november":
			monthNumber = 11;
			break;
		case "december":
			monthNumber = 12;
			break;
		default:
			monthNumber = 1;
			break;
		}
	}

	public int getMonthNumber() {
		return monthNumber;
	}
	
	public String getMonthName() {
		switch(monthNumber) {
		case 1:
			return "January";
		case 2:
			return "February";
		case 3:
			return "March";
		case 4:
			return "April";
		case 5:
			return "May";
		case 6:
			return "June";
		case 7:
			return "July";
		case 8:
			return "August";
		case 9:
			return "September";
		case 10:
			return "October";
		case 11:
			return "November";
		case 12:
			return "December";
		default:
			return "Error!!!";
		}
	}

	public String toString() {
		return getMonthName();
	}
	
	public boolean equals(Month month) {
		if(this.monthNumber == month.monthNumber)
			return true;
		else
			return false;
	}
	
	public boolean greaterThan(Month month) {
		if(this.monthNumber > month.monthNumber)
			return true;
		else
			return false;
	}
	
	boolean lessThan(Month month) {
		if(this.monthNumber < month.monthNumber)
			return true;
		else
			return false;
	}
}
