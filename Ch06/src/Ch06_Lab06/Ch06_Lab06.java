package Ch06_Lab06;

public class Ch06_Lab06 {

	public static void main(String[] args) {
		Employee worker1 = new Employee();
		Employee worker2 = new Employee("Test Dummy", 9999);
		Employee worker3 = new Employee("Other Dummy", 10101, "That one place", "Horizontal");
	
		displayWorkerInfo(worker1);
		displayWorkerInfo(worker2);
		displayWorkerInfo(worker3);
	}

	private static void displayWorkerInfo(Employee worker) {
		System.out.println("Employee Name: " + worker.getName());
		System.out.println("Employee ID: " + worker.getIdNumber());
		System.out.println("Employee Department: " + worker.getDepartment());
		System.out.println("Employee Position: " + worker.getPosition());
		System.out.println();
	}
}
