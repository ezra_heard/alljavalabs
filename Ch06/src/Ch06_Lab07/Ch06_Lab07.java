package Ch06_Lab07;

public class Ch06_Lab07 {

	public static void main(String[] args) {
		RetailItem item = new RetailItem("Candy bar", 17789, 0.75, 1.5);
		
		System.out.println(item);
		
		item.setWholesale(0.85);
		item.setRetail(2.99);
		
		System.out.println(item);
	}

}
