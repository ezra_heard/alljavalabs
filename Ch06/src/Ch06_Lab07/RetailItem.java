package Ch06_Lab07;

public class RetailItem {
	private String description;
	private int itemNumber;
	private CostData cost;
	
	public RetailItem(String desc, int itemNum, double wholesale, double retail) {
		description = desc;
		itemNumber = itemNum;
		cost = new CostData(wholesale, retail);
	}
	
	public String toString() {
		String str;
		
		str = String.format("Description: %s\nItem Number: %d\nWholesale Cost: $%,.2f\nRetail Price: $%,.2f\n", description, itemNumber, cost.wholesale, cost.retail);
	
		return str;
	}
	
	public double getWholesale() { return cost.wholesale; }
	public void setWholesale(double wholesale) { cost.wholesale = wholesale; }
	
	public double getRetail() { return cost.retail; }
	public void setRetail(double retail) { cost.retail = retail; }
	
	private class CostData
	{
		public double wholesale, retail;
		
		public CostData(double w, double r) {
			wholesale = w;
			retail = r;
		}
	}
}
