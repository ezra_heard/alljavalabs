package Ch06_Lab08;

import Ch06_Lab07.RetailItem;

public class CashRegister {
	private RetailItem item;
	private int quantity;
	
	public CashRegister(RetailItem item, int quantity) {
		this.item = item;
		this.quantity = quantity;
	}
	
	public double getSubtotal() {
		return quantity * item.getRetail();
	}
	
	public double getTax() {
		return this.getSubtotal() * 0.06;
	}
	
	public double getTotal() {
		return this.getSubtotal() + this.getTax();
	}
}
