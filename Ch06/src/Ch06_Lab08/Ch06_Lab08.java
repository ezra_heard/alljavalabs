package Ch06_Lab08;

import Ch06_Lab07.RetailItem;
import java.util.Scanner;

public class Ch06_Lab08 {

	public static void main(String[] args) {
		RetailItem item = new RetailItem("Candy bar", 17789, 0.75, 1.5);
		CashRegister purchase;
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.println(item);
		
		System.out.print("How many of these are you buying? ");
		purchase = new CashRegister(item, userInput.nextInt());
		
		System.out.println("\nYour Purchase: ");
		System.out.printf("Subtotal: $%,.2f\n", purchase.getSubtotal());
		System.out.printf("Tax: $%,.2f\n", purchase.getTax());
		System.out.printf("Total: $%,.2f\n", purchase.getTotal());
		
		userInput.close();
	}

}
