package Ch06_Lab09;

import java.util.Scanner;
import java.io.*;

import Ch06_Lab07.RetailItem;
import Ch06_Lab08.CashRegister;

public class Ch06_Lab09 {

	public static void main(String[] args) throws IOException {
		RetailItem item = new RetailItem("Candy bar", 17789, 0.75, 1.5);
		CashRegister purchase;
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.println(item);
		
		System.out.print("How many of these are you buying? ");
		int quantity = userInput.nextInt();
		purchase = new CashRegister(item, quantity);
		
		System.out.println("\nYour Purchase: ");
		System.out.printf("Subtotal: $%,.2f\n", purchase.getSubtotal());
		System.out.printf("Tax: $%,.2f\n", purchase.getTax());
		System.out.printf("Total: $%,.2f\n", purchase.getTotal());
		
		userInput.close();
		
		PrintWriter outputFile = new PrintWriter("src/Ch06_Lab09/receipt.txt");
		
		outputFile.println("SALES RECEIPT");
		outputFile.println("Unit Price: $" + String.format("%,.2f", item.getRetail()));
		outputFile.println("Quantity: " + quantity);
		outputFile.println("Subtotal: $" + String.format("%,.2f", purchase.getSubtotal()));
		outputFile.println("Sales Tax: $" + String.format("%,.2f", purchase.getTax()));
		outputFile.println("Total: $" + String.format("%,.2f", purchase.getTotal()));
		
		outputFile.close();
	}

}
