package Ch06_Lab10;

public class Ch06_Lab10 {

	public static void main(String[] args) {
		ParkedCar legalCar = new ParkedCar("Ford", "2010 Ford Mustang GT", "Sky Blue", "LL6-E8Y", 180);
		ParkedCar illegalCar = new ParkedCar("Saleen", "Saleen S7", "Hornet Yellow", "KL7-A3L", 1800);
		
		ParkingMeter meter1 = new ParkingMeter(210);
		ParkingMeter meter2 = new ParkingMeter(300);
		
		PoliceOfficer officer = new PoliceOfficer("Bob Smith", "099201");
		
		if(officer.checkParkedCar(legalCar, meter1)) {
			System.out.println("Car 1 is parked Illegally");
			System.out.println(officer.issueTicket(legalCar, meter1));
		}
		else
			System.out.println("Car 1 is parked Legally");
		
		System.out.println();
		
		if(officer.checkParkedCar(illegalCar, meter2)) {
			System.out.println("Car 2 is parked Illegally");
			System.out.println(officer.issueTicket(illegalCar, meter2));
		}
		else
			System.out.println("Car 2 is parked Legally");
	}

}
