package Ch06_Lab10;

public class ParkedCar {
	private String make;
	private String model;
	private String color;
	private String licensePlate;
	private int minutesParked;
	
	public ParkedCar(String make, String model, String color, String licensePlate, int minutesParked) {
		this.make = make;
		this.model = model;
		this.color = color;
		this.licensePlate = licensePlate;
		this.minutesParked = minutesParked;
	}
	
	public String getMake() { return make; }
	public String getModel() { return model; }
	public String getColor() { return color; }
	public String getLicensePlate() { return licensePlate; }
	public int getMinutesParked() { return minutesParked; }
}
