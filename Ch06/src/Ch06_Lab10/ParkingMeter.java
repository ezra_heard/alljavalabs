package Ch06_Lab10;

public class ParkingMeter {
	private int minutes;
	
	public ParkingMeter(int minutes) {
		this.minutes = minutes;
	}
	
	public int getMinutes() { return minutes; }
}
