package Ch06_Lab10;

public class ParkingTicket {
	public String reportCar(ParkedCar car) {
		return "Illegally Parked Car: " +
				"\n\tMake: " + car.getMake()+
				"\n\tModel: " + car.getModel() +
				"\n\tColor: " + car.getColor() +
				"\n\tPlate: " + car.getLicensePlate();
	}
	
	public double reportFine(ParkedCar car, ParkingMeter meter) {
		int minutes = car.getMinutesParked() - meter.getMinutes();
		double fine = 25.00;
		
		minutes -= 60;
		
		for(; minutes > 0; minutes -= 60) {
			fine += 10;
		}
		
		return fine;
	}
	
	public String reportOfficer(PoliceOfficer officer) {
		return "Officer " + officer.getName() + ", Badge Number: " + officer.getBadgeNumber();
	}
}
