package Ch06_Lab10;

public class PoliceOfficer {
	private String name;
	private String badgeNumber;
	
	public PoliceOfficer(String name, String badgeNumber) {
		this.name = name;
		this.badgeNumber = badgeNumber;
	}

	public String getName() { return name; }

	public String getBadgeNumber() { return badgeNumber; }

	public boolean checkParkedCar(ParkedCar car, ParkingMeter meter) {
		if(car.getMinutesParked() > meter.getMinutes())
			return true;
		else
			return false;
	}
	
	public String issueTicket(ParkedCar car, ParkingMeter meter){
		ParkingTicket ticket = new ParkingTicket();
		
		return "This car is fined for $" + String.format("%.2f", ticket.reportFine(car, meter) ) +
				"\n" + ticket.reportCar(car) +
				"\n" + ticket.reportOfficer(this);
	}
}
