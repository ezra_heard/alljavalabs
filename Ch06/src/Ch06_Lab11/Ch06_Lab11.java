package Ch06_Lab11;

import java.util.Scanner;

public class Ch06_Lab11 {

	public static void main(String[] args) {
		int choice = 0;
		
		Scanner userInput = new Scanner(System.in);
		
		for(; choice != 4;) {
			System.out.println("Geometry Calculator");
			System.out.println("1. Calculate the Area of a Circle");
			System.out.println("2. Calculate the Area of a Rectangle");
			System.out.println("3. Calculate the Area of a Triangle");
			System.out.println("4. Quit");
			System.out.print("Enter your choice (1-4): ");
			choice = userInput.nextInt();
			
			if(choice == 1) {
				double result = -1;
				
				for(; result < 0; ) {
					System.out.print("Enter circle radius: ");
					double radius = userInput.nextDouble();
					result = Geometry.calcCircleArea(radius);
				}
				
				System.out.printf("The circles area is: %.2f\n\n", result);
			}
			else if(choice == 2) {
				double result = -1;
				
				for(; result < 0; ) {
					System.out.print("Enter rectangle length: ");
					double length = userInput.nextDouble();
					
					System.out.print("Enter rectangle width: ");
					double width = userInput.nextDouble();
					result = Geometry.calcRectangleArea(length, width);
				}
				
				System.out.println("The rectangles area is: " + result + "\n");
			}
			else if(choice == 3) {
				double result = -1;
				
				for(; result < 0; ) {
					System.out.print("Enter triangle base: ");
					double base = userInput.nextDouble();
					
					System.out.print("Enter triangle height: ");
					double height = userInput.nextDouble();
					result = Geometry.calcTriangleArea(base, height);
				}
				
				System.out.println("The triangles area is: " + result + "\n");
			}
			else if(choice != 4) {
				System.out.print("\nPlease enter a valid menu choice\n\n");
			}
		}
		
		userInput.close();
		
	}
}
