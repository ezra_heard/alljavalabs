package Ch06_Lab11;

public class Geometry {

	public static double calcCircleArea(double radius) {
		if(radius > 0)
			return Math.PI * (radius * radius);
		else
			System.out.println("Error: positive numbers only please");
		
		return -1;
	}
	
	public static double calcRectangleArea(double length, double width) {
		if(length > 0 && width > 0)
			return length * width;
		else
			System.out.println("");
		
		return -1;
	}
	
	public static double calcTriangleArea(double base, double height) {
		if(base > 0 && height > 0)
			return base * height * 0.5;
		else
			System.out.println("");
		
		return -1;
	}
}
