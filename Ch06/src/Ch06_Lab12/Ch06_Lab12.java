package Ch06_Lab12;

public class Ch06_Lab12 {

	public static void main(String[] args) {
		FuelGauge tank = new FuelGauge(4);
		Odometer odometer = new Odometer(1200);
		
		System.out.println("Starting Fuel: " + tank.getGasAmount());
		System.out.println("Starting Mileage: " + odometer.getCurrentMileage());

		System.out.println();
		System.out.println("Filling Tank");
		
		for(; tank.getGasAmount() < 15; ) {
			tank.fillGallon();
			System.out.printf("Fuel: %02d | Mileage: %d\n", tank.getGasAmount(), odometer.getCurrentMileage());
		}
		
		System.out.println();
		System.out.println("Driving Car");
		
		for(; tank.getGasAmount() > 0; ) {
			odometer.TravelMile(tank);
			System.out.printf("Fuel: %02d | Mileage: %d\n", tank.getGasAmount(), odometer.getCurrentMileage());
		}
	}

}
