package Ch06_Lab12;

public class FuelGauge {
	private int gasAmount;
	
	public FuelGauge(int gasAmount) {
		this.gasAmount = gasAmount;
	}
	
	public int getGasAmount() { return gasAmount; }
	
	public void fillGallon() {
		if(gasAmount < 15)
			++gasAmount;
		else
			System.out.println("Tank is full.");
	}
	
	public void burnGallon() {
		if(gasAmount > 0)
			--gasAmount;
		else
			System.out.println("Tank is empty.");
	}
}
