package Ch06_Lab12;

public class Odometer {
	private int currentMileage;
	private int milesAccumulator = 0;

	public Odometer(int currentMileage) {
		this.currentMileage = currentMileage;
	}
	
	public int getCurrentMileage() { return currentMileage; }
	
	public void TravelMile(FuelGauge tank) {
		++currentMileage;
		++milesAccumulator;
		
		if(currentMileage > 999999)
			currentMileage = 0;
		
		if(milesAccumulator % 24 == 0)
			tank.burnGallon();
	}
}
