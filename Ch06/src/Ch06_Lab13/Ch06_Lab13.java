package Ch06_Lab13;

public class Ch06_Lab13 {

	public static void main(String[] args) {
		Player player1 = new Player("Ezra", 50);
		Player player2 = new Player("Doug", 50);
		
		Die d6 = new Die(6);
		
		System.out.println("Starting Scores>> " + player1.toString() + " | " + player2.toString());
		System.out.println();
		
		for(int i = 1; player1.getPoints() != 1 && player2.getPoints() != 1; ++i) {
			if(i % 2 != 0) {
				player1.rollDice(d6);
				System.out.println(player1.getName() + " rolled " + d6.getValue());
			}
			else {
				player2.rollDice(d6);
				System.out.println(player2.getName() + " rolled " + d6.getValue());
			}
			
			System.out.println("Turn " + i + ">> " + player1.toString() + " | " + player2.toString());
			System.out.println();
		}
		
		System.out.print("Game Over: ");
		
		if(player1.getPoints() == 1)
			System.out.println(player1.getName() + " Wins!!!");
		else
			System.out.println(player2.getName() + " Wins!!!");
	}

}
