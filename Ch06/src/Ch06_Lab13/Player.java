package Ch06_Lab13;

public class Player {
	private int points;
	private String name;
	
	public Player(String name, int points) {
		this.name = name;
		this.points = points;
	}
	
	public int getPoints() { return points; }
	public String getName() { return name; }

	public void rollDice(Die die) {
		die.roll();
		
		if(points - die.getValue() > 0)
			points -= die.getValue();
		else
			points += die.getValue();
	}
	
	public String toString() {
		return name + ": " + points + " pts";
	}
}
