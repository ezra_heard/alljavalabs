package Ch06_Lab14;

public class Ch006_Lab14 {

	public static void main(String[] args) {
		Player player1 = new Player("Ezra", 0);
		Player player2 = new Player("Doug", 0);
		
		Coin d2 = new Coin();
		
		for(int i = 1; player1.getCorrect() < 5 && player2.getCorrect() < 5; ++i) {
			d2.toss();
			
			player1.guess(d2.getSideUp());
			player2.guess(d2.getSideUp());
			
			System.out.println("Turn " + i + ":");
			System.out.println("\t" + "The coin landed on " + d2.getSideUp());
			System.out.println("\t" + player1.getName() + " guessed " + (player1.getPlayerGuess() == 0 ? "heads" : "tails"));
			System.out.println("\t" + player2.getName() + " guessed " + (player2.getPlayerGuess() == 0 ? "heads" : "tails"));
			System.out.println(player1.toString() + " | " + player2.toString());
			System.out.println();
		}
		
		System.out.print("Game over: ");
		
		if(player1.getCorrect() == 5 && player2.getCorrect() == 5)
			System.out.println(player1.getName() + " and " + player2.getName() + " tie!");
		else if(player1.getCorrect() == 5)
			System.out.println(player1.getName() + " wins!!!");
		else if(player2.getCorrect() == 5)
			System.out.println(player2.getName() + " wins!!!");
			
	}

}
