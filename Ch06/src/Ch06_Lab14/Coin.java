package Ch06_Lab14;

public class Coin {
	private String sideUp;
	
	public Coin() {
		toss();
	}
	
	public String getSideUp() { return sideUp; }
	
	public void toss() {
		int result = (int)Math.round(Math.random());
		
		if(result == 0)
			sideUp = "heads";
		else if(result == 1)
			sideUp = "tails";
	}
}
