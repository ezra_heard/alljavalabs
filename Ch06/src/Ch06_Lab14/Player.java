package Ch06_Lab14;

public class Player {
	private int correct;
	private String name;
	private int playerGuess;
	
	public Player(String name, int points) {
		this.name = name;
		this.correct = points;
	}
	
	public int getCorrect() { return correct; }
	public String getName() { return name; }
	public int getPlayerGuess() { return playerGuess; }
	
	public void guess(String side) {
		playerGuess = (int)Math.round(Math.random());
		
		if(side == "heads" && playerGuess == 0)
			++correct;
		else if(side == "tails" && playerGuess == 1)
			++correct;
	}
	
	public String toString() {
		return name + ": " + correct + " correct";
	}
}
