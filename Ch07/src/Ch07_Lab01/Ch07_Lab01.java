package Ch07_Lab01;

public class Ch07_Lab01 {
	public static void main(String[] args) {
		//RainFall rainfall = new RainFall(new double[] {12, 57.5, 333, 412, 85.7, 28, 3.14, 99.99, 101, 50, 99, -999});
		RainFall rainfall = new RainFall(new double[] {8, 6, 10, 5, 2, 1, 2, 2, 5, 9, 11, 12});
		
		System.out.printf("Total Rainfall: %.2f/mm of rain\n", rainfall.getTotalRainfall() );
		System.out.printf("Average Rainfall: %.2f/mm of rain\n", rainfall.getAverageRainfall());
		System.out.printf("Greatest Rainfall: Month %d with %.2f/mm of rain\n", rainfall.getGreatestRainfall() + 1, rainfall.getRainfall(rainfall.getGreatestRainfall()));
		System.out.printf("Least Rainfall (not 0): Month %d with %.2f/mm of rain\n", rainfall.getLeastRainfall() + 1, rainfall.getRainfall(rainfall.getLeastRainfall()));
	}
}