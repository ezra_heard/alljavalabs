package Ch07_Lab01;

public class RainFall {
	private double[] monthlyRainfall;
	
	public RainFall(double[] monthlyRainfall) {
		this.monthlyRainfall = new double[monthlyRainfall.length];
		
		for(int i = 0; i < monthlyRainfall.length; ++i) {
			//Don't Allow Negative Numbers for monthly Rainfall
			if(monthlyRainfall[i] >= 0)
				this.monthlyRainfall[i] = monthlyRainfall[i];
			else
				this.monthlyRainfall[i] = 0;
		}
	}
	
	public double getTotalRainfall(){
		double total = 0;
		
		for(double monthsRain : this.monthlyRainfall) {
			total += monthsRain;
		}
		
		return total;
	}
	
	public double getAverageRainfall() {
		return this.getTotalRainfall() / this.monthlyRainfall.length;
	}
	
	public int getGreatestRainfall(){
		double highest = 0;
		int month = -1;
		
		for(int i = 0; i < this.monthlyRainfall.length; ++i) {
			if(this.monthlyRainfall[i] > highest) {
				highest = this.monthlyRainfall[i];
				month = i;
			}
		}
		
		return month;
	}
	
	public int getLeastRainfall(){
		double lowest = Double.MAX_VALUE;
		int month = -1;
		
		for(int i = 0; i < this.monthlyRainfall.length; ++i) {
			if(this.monthlyRainfall[i] < lowest && this.monthlyRainfall[i] != 0) {
				lowest = this.monthlyRainfall[i];
				month = i;
			}
		}
		
		return month;
	}
	
	public double getRainfall(int position) {
		return this.monthlyRainfall[position];
	}
}
