package Ch07_Lab02;

import java.util.Scanner;

public class Ch07_Lab02 {

	public static void main(String[] args) {
		int[] hours = new int[7];
		double[] payRate = new double[7];
		
		Payroll payroll = new Payroll();
		
		Scanner userInput = new Scanner(System.in);
		
		
		for(int i = 0; i < payroll.getEmployeeId().length; ++i) {
			do {
				System.out.print("Enter employee " + payroll.getEmployeeId()[i] + "'s hours: ");
				hours[i] = userInput.nextInt();
				
				if(hours[i] < 0)
					System.out.println("Invlid number: hours can't be negative");
				
			}while(hours[i] < 0);
			
			do {
				System.out.print("Enter employee " + payroll.getEmployeeId()[i] + "'s payrate: ");
				payRate[i] = userInput.nextDouble();
				
				if(payRate[i] < 6)
					System.out.println("Invlid number: payrate can't below $6.00/hr");
				
			}while(payRate[i] < 6);
			
			System.out.println();
		}
		
		//Reassign payroll object to fresh values
		payroll = new Payroll(hours, payRate);
		
		//Wipe old arrays (might speed up garbage collection)
		hours = null; payRate = null;
		
		for(int i = 0; i < payroll.getEmployeeId().length; ++i) {
			System.out.printf("Employee %d's Gross Pay: $%,.2f\n", payroll.getEmployeeId()[i], payroll.getGrossPay(payroll.getEmployeeId()[i]));
		}
		
		userInput.close();
	}

}
