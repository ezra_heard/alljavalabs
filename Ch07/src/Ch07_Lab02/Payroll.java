package Ch07_Lab02;

public class Payroll {
	private int[] employeeId = {5658845, 4520125, 7895122, 8777541, 8451277, 1302850, 7580489};
	private int[] hours;
	private double[] payRate;
	private double[] wages;
	
	public Payroll() {
		this.hours = new int[7];
		this.payRate = new double[7];
		this.wages = new double[7];
	}
	
	public Payroll(int[] hours, double[] payRate) {
		this.hours = new int[7];
		this.payRate = new double[7];
		this.wages = new double[7];
		
		for(int i = 0; i < this.hours.length; ++i) {
			this.hours[i] = hours[i];
			this.payRate[i] = payRate[i];
		}	
		
		calcGrossPay();
	}
	
	//Getters and Setters for Whole Arrays
	public int[] getEmployeeId() {
		int[] temp = new int[7];
		
		for(int i = 0; i < temp.length; ++i) {
			temp[i] = employeeId[i];
		}
		
		return temp;
	}

	public void setEmployeeId(int[] employeeId) {
		for(int i = 0; i < this.employeeId.length; ++i)
			this.employeeId[i] = employeeId[i];
	}

	public int[] getHours() {
		int[] temp = new int[7];
		
		for(int i = 0; i < temp.length; ++i) {
			temp[i] = hours[i];
		}
		
		return temp;
	}

	public void setHours(int[] hours) {
		for(int i = 0; i < this.hours.length; ++i)
			this.hours[i] = hours[i];
		
		calcGrossPay();
	}

	public double[] getPayRate() {
		double[] temp = new double[7];
		
		for(int i = 0; i < temp.length; ++i) {
			temp[i] = payRate[i];
		}
		
		return temp;
	}

	public void setPayRate(double[] payRate) {
		for(int i = 0; i < this.payRate.length; ++i)
			this.payRate[i] = payRate[i];
		
		calcGrossPay();
	}

	public double[] getWages() {
		double[] temp = new double[7];
		
		for(int i = 0; i < temp.length; ++i) {
			temp[i] = wages[i];
		}
		
		return temp;
	}

	//Overloaded Setters by Index
	public void setEmpolyeeId(int employeeId , int index) {
		this.employeeId[index] = employeeId;
	}
	
	public void setHours(int hours , int index) {
		this.hours[index] = hours;
		
		calcGrossPay();
	}

	public void setPayRate(int payRate , int index) {
		this.payRate[index] = payRate;
		
		calcGrossPay();
	}
	
	//Gets the gross pay for an employee based on their ID
	public double getGrossPay(int employeeId) {
	
		for(int i = 0; i < this.employeeId.length; ++i) {
			if(this.employeeId[i] == employeeId)
				return this.wages[i];
		}
		
		return 0;
	}
	
	
	//Calculates the Gross Pay and assigns its values to the array
	private void calcGrossPay() {
		for(int i = 0; i < this.wages.length; ++i) {
			this.wages[i] = this.hours[i] * this.payRate[i];
		}
	}
}
