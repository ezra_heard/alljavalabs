package Ch07_Lab03;

import java.util.Scanner;

public class Ch07_Lab03 {

	public static void main(String[] args) {
		int accountNum;
		
		ChargeAccount account = new ChargeAccount();
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.print("Enter Account Number: ");
		
		accountNum = userInput.nextInt();
		
		System.out.print(accountNum + " is ");
		
		if(account.chackValidAccount(accountNum))
			System.out.print("a valid");
		else
			System.out.print("an invalid");
		
		System.out.println(" account number");
		
		userInput.close();
	}
}
