package Ch07_Lab04;

public class Ch07_Lab04 {

	public static void main(String[] args) {
		getLargerThan(new int[] {99, 22, 124, 125, 30, 335, 1, 1, 59},  50);
	}
	
	private static void getLargerThan(int[] numbers, int n) {
		for(int i : numbers) {
			if(i > n)
				System.out.println(i);
		}
	}
}
