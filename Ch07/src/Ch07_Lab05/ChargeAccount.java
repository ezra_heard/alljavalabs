package Ch07_Lab05;

import java.io.*;
import java.util.Scanner;

public class ChargeAccount{
	private int[] accounts;

	public ChargeAccount() throws IOException{
		accounts = new int[18];
		
		File file = new File("src/Ch07_Lab05/AccountNumbers.txt");
		Scanner inputFile = new Scanner(file);
		
		for(int i = 0; inputFile.hasNext() && i < accounts.length; ++i) {
			accounts[i] = inputFile.nextInt();
		}
		
		inputFile.close();
	}
	
	
	public boolean chackValidAccount(int accountNum) {
		for(int account : accounts) {
			if(accountNum == account)
				return true;
		}
	
		return false;
	}
}
