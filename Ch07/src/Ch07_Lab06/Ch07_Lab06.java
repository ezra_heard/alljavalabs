package Ch07_Lab06;

import java.util.Scanner;

public class Ch07_Lab06 {

	public static void main(String[] args) {
		char[] answers = new char[20];
		
		DriverExam exam;
		
		Scanner userInput = new Scanner(System.in);
	
		for(int i = 0; i < answers.length; ++i) {
			String letter = "";
			
			while(!letter.equalsIgnoreCase("a") &&
				  !letter.equalsIgnoreCase("b") &&
				  !letter.equalsIgnoreCase("c") &&
				  !letter.equalsIgnoreCase("d")) {
				System.out.print("Enter answer " + (i + 1) + ": ");
				letter = userInput.nextLine();
				
				if(!letter.equalsIgnoreCase("a") &&
				   !letter.equalsIgnoreCase("b") &&
				   !letter.equalsIgnoreCase("c") &&
				   !letter.equalsIgnoreCase("d"))
				System.out.println("Invalid letter, please try again (a, b, c, d)");
			}
			
			answers[i] = letter.toUpperCase().charAt(0);
		}
		
		exam = new DriverExam(answers);
		
		System.out.println();
		System.out.println(exam.passed() ? "Passed Exam" : "Failed Exam");
		System.out.println("Got " + exam.totalCorrect() + " correct");
		System.out.println("Got " + exam.totalIncorrect() + " incorrect");
		System.out.print("Missed questions: ");
		
		int[] missed = exam.questionsMissed();
		
		for(int i = 0; i < missed.length; ++i) {
			System.out.print(missed[i] + 1);
			
			if(i != missed.length - 1)
				System.out.print(", ");
		}
		
		userInput.close();
	}

}
