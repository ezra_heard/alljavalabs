package Ch07_Lab06;

public class DriverExam {
	private char[] studentAnswers;
	private char[] correctAnswers = {'B', 'D', 'A', 'A', 'C',
									 'A', 'B', 'A', 'C', 'D',
									 'B', 'C', 'D', 'A', 'D',
									 'C', 'C', 'B', 'D', 'A'};
	
	
	public DriverExam(char[] studentAnswers) {
		this.studentAnswers = new char[studentAnswers.length];
		
		for(int i = 0; i < this.studentAnswers.length; ++i)
			this.studentAnswers[i] = studentAnswers[i];
	}

	public boolean passed() {
		if(totalCorrect() >= 15)
			return true;
		else
			return false;
	}
	
	public int totalCorrect() {
		int correct = 0;
		
		for(int i = 0; i < correctAnswers.length; ++i) {
			if(correctAnswers[i] == studentAnswers[i])
				++correct;
		}
		
		return correct;
	}
	
	public int totalIncorrect() {
		int incorrect = 0;
		
		for(int i = 0; i < correctAnswers.length; ++i) {
			if(correctAnswers[i] != studentAnswers[i])
				++incorrect;
		}
		
		return incorrect;
	}
	
	public int[] questionsMissed() {
		int[] questionsMissed = new int[totalIncorrect()];
		int missedIndex = 0;
		
		for(int i = 0; i < correctAnswers.length; ++i) {
			if(correctAnswers[i] != studentAnswers[i]) {
				questionsMissed[missedIndex] = i;
				++missedIndex;
			}
		}
		
		return questionsMissed;
	}
}
