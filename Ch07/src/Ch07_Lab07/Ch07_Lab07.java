package Ch07_Lab07;

import java.io.*;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Random;

public class Ch07_Lab07{

	public static void main(String[] args) throws IOException{
		boolean sentinal = true;
		
		Random rand = new Random();
		
		ArrayList<String> eightBallReplyList = new ArrayList<String>();
		
		File file = new File("src/Ch07_Lab07/8_ball_responses.txt");
		Scanner inputFile = new Scanner(file);
		Scanner userInput = new Scanner(System.in);
		
		while(inputFile.hasNext())
			eightBallReplyList.add(inputFile.nextLine());
		
		
		
		while(sentinal) {
			System.out.println("What is your question?");
			userInput.nextLine();
			
			System.out.println(eightBallReplyList.get(rand.nextInt(eightBallReplyList.size())));
		
			System.out.println();
			
			System.out.println("Would you like to another? (y/n)");
			String answer = userInput.nextLine().toUpperCase();
			
			System.out.println();
			
			if(answer.charAt(0) != 'Y')
				sentinal = false;
		}
		
		inputFile.close();
		userInput.close();
	}

}
