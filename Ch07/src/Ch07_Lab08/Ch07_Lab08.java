package Ch07_Lab08;

import java.util.Scanner;

public class Ch07_Lab08 {

	public static void main(String[] args) {
		String[] students = new String[5];
		double[][] testScores = new double[5][4];
		
		GradeBook gradeBook;
		
		Scanner userInput = new Scanner(System.in);
		
		for(int i = 0; i < students.length; ++i) {
			System.out.print("Enter Student " + (i + 1) + "'s Name: " );
			students[i] = userInput.nextLine();
			
			testScores[i][0] = getValidScore(1, userInput);
			testScores[i][1] = getValidScore(2, userInput);
			testScores[i][2] = getValidScore(3, userInput);
			testScores[i][3] = getValidScore(4, userInput);
			
			userInput.nextLine(); //Clear the keyboard buffer
			System.out.println();
		}
		
		gradeBook = new GradeBook(students, testScores);
		
		System.out.println(gradeBook);
		
		userInput.close();
	}

	private static double getValidScore(int i, Scanner userInput) {
		double num = -1;
		
		while(num < 0 || num > 100) {
			System.out.print("Enter Score " + i + ": ");
			num = userInput.nextDouble();
			
			if(num < 0 || num > 100)
				System.out.println("Invalid Number: Score must be between 0 and 100");
		}
		
		return num;
		
	}
	
}
