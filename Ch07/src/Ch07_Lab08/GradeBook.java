package Ch07_Lab08;

public class GradeBook {
	private String[] students = new String[5];
	private char[] letterGrades = new char[5];
	private double[][] testScores = new double[5][4];
	
	public GradeBook(String[] students, double[][] testScores) {
		for(int i = 0; i < this.students.length; ++i) {
			this.students[i] = students[i];
			this.testScores[i][0] = testScores[i][0];
			this.testScores[i][1] = testScores[i][1];
			this.testScores[i][2] = testScores[i][2];
			this.testScores[i][3] = testScores[i][3];
			
			if(getAverageScore(i) >= 90)
				this.letterGrades[i] = 'A';
			else if(getAverageScore(i) >= 80)
				this.letterGrades[i] = 'B';
			else if(getAverageScore(i) >= 70)
				this.letterGrades[i] = 'C';
			else if(getAverageScore(i) >= 60)
				this.letterGrades[i] = 'D';
			else if(getAverageScore(i) >= 0)
				this.letterGrades[i] = 'F';
			else
				System.out.println("An error occured, average score less than 0");
		}
	}
	
	public String getStudents(int index) {
		return students[index];
	}

	public char getLetterGrades(int index) {
		return letterGrades[index];
	}

	private double getAverageScore(int index) {
		return (testScores[index][0] + testScores[index][1] + testScores[index][2] + testScores[index][3]) / 4;
	}
	
	@Override
	public String toString() {
		String returnString = "";
		
		for(int i = 0; i < students.length; ++i)
			returnString += "Name: " + students[i] + "\n\tAverage Score: " + String.format("%.2f", getAverageScore(i)) + "\n\tLetter Grade: " + letterGrades[i] + "\n\n";
	
		return returnString;
	}
}
