package Ch07_Lab10;

import java.io.*;
import java.util.Scanner;

public class Ch07_Lab10 {

	public static void main(String[] args) throws IOException {
		int[] monthDays = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
		int[] monthSteps = new int[12];
		
		File file = new File("src/Ch07_Lab10/steps.txt");
		Scanner inputFile = new Scanner(file);
		
		for(int i = 0; i < monthSteps.length; ++i) {
			monthSteps[i] = 0;
			
			for(int x = 0; x < monthDays[i]; ++x) {
				monthSteps[i] += inputFile.nextInt();
			}
			
			System.out.println("Month " + (i + 1) + " average: " + String.format("%.2f", ((double)monthSteps[i] / monthDays[i])));
		}
		
		inputFile.close();
	}

}
