package SelectionSortExample;

import java.util.Random;

public class SelectionSortExample {

	public static void main(String[] args) {
		Random rand = new Random();
		
		int numbers[] = new int[10];
		
		for(int i = 0; i < numbers.length; ++i){
			numbers[i] = rand.nextInt(100) + 1;
		}
		
		int startScan, index, minIndex, minValue;
		
		for (startScan = 0; startScan < (numbers.length-1); startScan++)
		{
			minIndex = startScan;
			minValue = numbers[startScan];
			
			for(index = startScan + 1; index < numbers.length; index++)
			{
				if (numbers[index] < minValue)
				{
					minValue = numbers[index];
					minIndex = index;
				}
			}
			numbers[minIndex] = numbers[startScan];
			numbers[startScan] = minValue;
		}
	}

}
