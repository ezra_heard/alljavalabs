package Ch09_Lab01;

public class Ch09_Lab01 {

	public static void main(String[] args) {
		ProductionWorker employee1 = new ProductionWorker("James", "123-J", "12/12/2012", 1, 8.75);
		ProductionWorker employee2 = new ProductionWorker();

		employee2.setName("Janice");
		employee2.setEmployeeNumber("99911");
		employee2.setHireDate("9/09/1999");
		employee2.setShift(0);
		employee2.setHourlyPay(9.99);
		
		System.out.println(employee1.toString());
		System.out.println();
		System.out.println(employee2.toString());
	}
}
