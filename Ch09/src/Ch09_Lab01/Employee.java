package Ch09_Lab01;

public class Employee {
	private String name;
	private String employeeNumber;
	private String hireDate;
	
	public Employee() {}

	public Employee(String name, String employeeNumber, String hireDate) {
		this.name = name;
		
		if(isValidEmployeeNumber(employeeNumber))
			this.employeeNumber = employeeNumber;
		else
			this.employeeNumber = "XXX-L";
		
		this.hireDate = hireDate;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setEmployeeNumber(String employeeNumber) {
		if(isValidEmployeeNumber(employeeNumber))
			this.employeeNumber = employeeNumber;
		else
			this.employeeNumber = "XXX-L";
	}

	public void setHireDate(String hireDate) {
		this.hireDate = hireDate;
	}
	
	public String getName() {
		return name;
	}

	public String getEmployeeNumber() {
		return employeeNumber;
	}

	public String getHireDate() {
		return hireDate;
	}
	
	private boolean isValidEmployeeNumber(String employeeNumber) {
		if(employeeNumber.length() != 5)
			return false;
		
		for(int i = 0; i < 3; ++i) {
			if(!Character.isDigit(employeeNumber.charAt(i)))	
				return false;
		}
		
		if(employeeNumber.charAt(3) != '-')
			return false;
			
		if(!Character.isLetter(employeeNumber.charAt(4)) || !employeeNumber.substring(4).matches("[a-mA-M]"))
			return false;
		
		return true;
	}
	
	public String toString() {
		return "Employee Name: " + this.name + "\nEmployee Number: " + this.employeeNumber + "\nHire Date: " + this.hireDate;
	}
}
