package Ch09_Lab01;

public class ProductionWorker extends Employee {
	private int shift;
	private double hourlyPay;
	
	public final int DAY_SHIFT = 1;
	public final int NIGHT_SHIFT = 2;
	
	public ProductionWorker() {}
	
	public ProductionWorker(int shift, double hourlyPay) {
		super();
		this.shift = shift;
		this.hourlyPay = hourlyPay;
	}
	
	public ProductionWorker(String name, String employeeNumber, String hireDate, int shift, double hourlyPay) {
		super(name, employeeNumber, hireDate);
		
		this.shift = shift;
		this.hourlyPay = hourlyPay;
	}

	public int getShift() {
		return shift;
	}

	public double getHourlyPay() {
		return hourlyPay;
	}

	public void setShift(int shift) {
		this.shift = shift;
	}

	public void setHourlyPay(double hourlyPay) {
		this.hourlyPay = hourlyPay;
	}
	
	@Override
	public String toString() {
		String shiftString;
		
		if(shift == DAY_SHIFT)
			shiftString = "Day";
		else if(shift == NIGHT_SHIFT)
			shiftString = "Night";
		else
			shiftString = "invalid";
			
		return super.toString() + "\nShift: " + shiftString + " shift\nPay Rate: $" + String.format("%,.2f", this.hourlyPay); 
	}
}
