package Ch09_Lab02;

public class Ch09_Lab02 {

	public static void main(String[] args) {
		ShiftSupervisor employee1 = new ShiftSupervisor("Mike", "456-P", "11/11/2011", 45000, 5000);
		ShiftSupervisor employee2 = new ShiftSupervisor();

		employee2.setName("Matt");
		employee2.setEmployeeNumber("204-1");
		employee2.setHireDate("8/08/1988");
		employee2.setSalary(55000);
		employee2.setBonus(10000);
		
		System.out.println(employee1.toString());
		System.out.println();
		System.out.println(employee2.toString());
	}

}
