package Ch09_Lab02;

import Ch09_Lab01.Employee;

public class ShiftSupervisor extends Employee{
	private double salary;
	private double bonus;
	
	public ShiftSupervisor() {}
	
	public ShiftSupervisor(String name, String employeeNumber, String hireDate, double salary, double bonus) {
		super(name, employeeNumber, hireDate);
		
		this.salary = salary;
		this.bonus = bonus;
	}

	public void setSalary(double salary) { this.salary = salary; }

	public void setBonus(double bonus) { this.bonus = bonus; }
	
	public double getSalary() { return salary; }

	public double getBonus() { return bonus; }
	
	@Override
	public String toString() {	
		return super.toString() + "\nSalary: $" + String.format("%,.2f", this.salary) + "\nYearly Bonus: $" + String.format("%,.2f", this.bonus); 
	}
}
