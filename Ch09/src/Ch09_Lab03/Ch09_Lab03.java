package Ch09_Lab03;

public class Ch09_Lab03 {

	public static void main(String[] args) {
		TeamLeader employee1 = new TeamLeader("James", "123-J", "12/12/2012", 1, 8.75, 300, 20, 15);
		TeamLeader employee2 = new TeamLeader();

		employee2.setName("Janice");
		employee2.setEmployeeNumber("99911");
		employee2.setHireDate("9/09/1999");
		employee2.setShift(4);
		employee2.setHourlyPay(9.99);
		employee2.setMonthlyBonus(500);
		employee2.setRequiredTrainingHours(30);
		employee2.setTrainingHoursAttended(12);
		
		System.out.println(employee1.toString());
		System.out.println();
		System.out.println(employee2.toString());
	}

}
