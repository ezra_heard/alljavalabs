package Ch09_Lab03;

import Ch09_Lab01.ProductionWorker;

public class TeamLeader extends ProductionWorker {
	private double monthlyBonus;
	private double requiredTrainingHours;
	private double trainingHoursAttended;
	
	public TeamLeader() {}
	
	public TeamLeader(String name, String employeeNumber, String hireDate, int shift, double hourlyPay,
			double monthlyBonus, double requiredTrainingHours, double trainingHoursAttended) {
		
		super(name, employeeNumber, hireDate, shift, hourlyPay);
		
		this.monthlyBonus = monthlyBonus;
		this.requiredTrainingHours = requiredTrainingHours;
		this.trainingHoursAttended = trainingHoursAttended;
	}

	public void setMonthlyBonus(double monthlyBonus) {
		this.monthlyBonus = monthlyBonus;
	}
	public void setRequiredTrainingHours(double requiredTrainingHours) {
		this.requiredTrainingHours = requiredTrainingHours;
	}
	public void setTrainingHoursAttended(double trainingHoursAttended) {
		this.trainingHoursAttended = trainingHoursAttended;
	}
	
	public double getMonthlyBonus() {
		return monthlyBonus;
	}
	public double getRequiredTrainingHours() {
		return requiredTrainingHours;
	}
	public double getTrainingHoursAttended() {
		return trainingHoursAttended;
	}
	
	@Override
	public String toString() {	
		return super.toString() + "\nMonthly Bonus: $" + String.format("%,.2f", this.monthlyBonus) + "\nRequired Training Hours: " + this.requiredTrainingHours + "\nTraining Hours Attended: " + this.trainingHoursAttended; 
	}
}
