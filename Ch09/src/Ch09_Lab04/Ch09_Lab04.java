package Ch09_Lab04;

public class Ch09_Lab04 {

	public static void main(String[] args) {
		Essay essay1 = new Essay();
		Essay essay2 = new Essay();
		Essay essay3 = new Essay();
		Essay essay4 = new Essay();
		
		//All correct values
		essay1.setScore(30, 20, 20, 30);
		
		essay2.setGrammer(25);
		essay2.setSpelling(15);
		essay2.setCorrectLength(15);
		essay2.setContent(25);
		
		//Some Incorrect Values
		essay3.setScore(50, -1, -999, 50);
		
		essay4.setGrammer(99);
		essay4.setSpelling(99);
		essay4.setCorrectLength(-99);
		essay4.setContent(-99);
		
		System.out.println("Grade for Essay 1: " + essay1.getGrade() + "\nScore: " + essay1.getScore() + "\n");
		System.out.println("Grade for Essay 2: " + essay2.getGrade() + "\nScore: " + essay2.getScore() + "\n");
		System.out.println("Grade for Essay 3: " + essay3.getGrade() + "\nScore: " + essay3.getScore() + "\n");
		System.out.println("Grade for Essay 4: " + essay4.getGrade() + "\nScore: " + essay4.getScore());
	}

}
