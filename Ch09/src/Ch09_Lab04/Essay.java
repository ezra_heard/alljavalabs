package Ch09_Lab04;

public class Essay extends GradedActivity{
	private double grammer = 0;
	private double spelling = 0;
	private double correctLength = 0;
	private double content = 0;
	
	public void setScore(double grammer, double spelling, double correctLength, double content) {
		this.grammer = this.checkScore(grammer, 30);
		this.spelling = this.checkScore(spelling, 20);
		this.correctLength = this.checkScore(correctLength, 20);
		this.content = this.checkScore(content, 30);
		
		this.setScore();	
	}

	public void setGrammer(double grammer) { this.grammer = this.checkScore(grammer, 30); setScore(); }

	public void setSpelling(double spelling) { this.spelling = this.checkScore(spelling, 20); setScore(); }

	public void setCorrectLength(double correctLength) { this.correctLength = this.checkScore(correctLength, 20); setScore(); }

	public void setContent(double content) { this.content = this.checkScore(content, 30); setScore(); }
	
	public double getGrammer() { return grammer; }

	public double getSpelling() { return spelling; }

	public double getCorrectLength() { return correctLength; }

	public double getContent() { return content; }
	
	public double getScore() {
		return super.getScore();
	}

	private double checkScore(double score, int max) {
		if(score >= 0 && score <= max)
			return score;
		else if(score < 0)
			return 0;
		else
			return max;
	}

	private void setScore() {
		super.setScore(this.grammer + this.spelling + this.correctLength + this.content);
	}
}
