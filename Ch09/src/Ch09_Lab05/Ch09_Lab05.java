package Ch09_Lab05;

import java.util.Scanner;

import Ch09_Lab04.GradedActivity;
import Ch09_Lab04.Essay;

public class Ch09_Lab05 {

	public static void main(String[] args) {
		GradedActivity score1 = new GradedActivity();;
		PassFailExam score2;
		Essay score3  = new Essay();
		FinalExam score4;
		CourseGrades scoreBook = new CourseGrades();
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.print("Enter Lab Score: ");
		score1.setScore(userInput.nextDouble());
		
		System.out.print("Enter number of questions missed on Pass Fail: ");
		score2 = new PassFailExam(10, userInput.nextInt(), 70);

		System.out.print("Enter Essay Grammer Score: ");
		score3.setGrammer(userInput.nextDouble());
		System.out.print("Enter Essay Spelling Score: ");
		score3.setSpelling(userInput.nextDouble());
		System.out.print("Enter Essay Correct Length Score: ");
		score3.setCorrectLength(userInput.nextDouble());
		System.out.print("Enter Essay Content Score: ");
		score3.setContent(userInput.nextDouble());
		
		System.out.print("Enter number of questions missed on Final Exam: ");
		score4 = new FinalExam(50, userInput.nextInt());
		
		scoreBook.setLab(score1);
		scoreBook.setPassFailExam(score2);
		scoreBook.setEssay(score3);
		scoreBook.setFinalExam(score4);
		
		System.out.println();
		System.out.println(scoreBook);
		
		userInput.close();
	}
}
