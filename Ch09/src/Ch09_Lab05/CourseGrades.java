package Ch09_Lab05;

import Ch09_Lab04.GradedActivity;
import Ch09_Lab04.Essay;

public class CourseGrades {
	private int NUM_GRADES = 4;
	private GradedActivity[] grades = new GradedActivity[NUM_GRADES];
	
	public CourseGrades(){}
	
	public void setLab(GradedActivity aLab) {
		GradedActivity temp = new GradedActivity();
		temp.setScore(aLab.getScore());
		
		grades[0] = temp;
	}
	
	public void setPassFailExam(PassFailExam aPassFailExam) {
		PassFailExam temp = new PassFailExam(10, aPassFailExam.getNumMissed(), 70);
		temp.setScore(aPassFailExam.getScore());
		
		grades[1] = temp;
	}
	
	public void setEssay(Essay anEssay) {
		Essay temp = new Essay();
		temp.setScore(anEssay.getGrammer(), anEssay.getSpelling(), anEssay.getCorrectLength(), anEssay.getContent());
		
		grades[2] = temp;
	}
	
	public void setFinalExam(FinalExam aFinalExam) {
		FinalExam temp = new FinalExam(50, aFinalExam.getNumMissed());
		temp.setScore(aFinalExam.getScore());
		
		grades[3] = temp;
	}
	
	@Override
	public String toString() {
		return "Lab Score: " + grades[0].getScore() + "\nLab Grade: " + grades[0].getGrade() +
				"\n\nPass Fail Score: " + grades[1].getScore() + "\nPass Fail Grade: " + grades[1].getGrade() +
				"\n\nEssay Score: " + grades[2].getScore() + "\nEssay Grade: " + grades[2].getGrade() + 
				"\n\nFinal Exam Score: " + grades[3].getScore() + "\nFinal Exam Grade: " + grades[3].getGrade();
	}
}
