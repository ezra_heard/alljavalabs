package Ch09_Lab05;

import Ch09_Lab04.GradedActivity;

public class FinalExam extends GradedActivity {

	private int numQuestions;
	private double pointsEach;
	private int numMissed;
	
	public FinalExam(int numQuestions, int numMissed) {
		double numericScore;
		
		this.numQuestions = numQuestions;
		this.numMissed = numMissed;
		
		pointsEach = 100.0 / this.numQuestions;
		numericScore = 100.0 - (this.numMissed * pointsEach);
		
		setScore(numericScore);
	}
	
	public double getPointsEach() {
		return pointsEach;
	}
	
	public int getNumMissed() {
		return numMissed;
	}
	
}
