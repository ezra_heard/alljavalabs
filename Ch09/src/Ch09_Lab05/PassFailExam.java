package Ch09_Lab05;

public class PassFailExam extends PassFailActivity {

	private int numQuestions;
	private double pointsEach;
	private int numMissed;
	
	public PassFailExam(int numQuestions, int numMissed, double minPassingScore) {
		super(minPassingScore);
		
		double numericScore;
		
		this.numQuestions = numQuestions;
		this.numMissed = numMissed;
		
		pointsEach = 100.0 / this.numQuestions;
		
		numericScore = 100.0 - (numMissed * pointsEach);
		
		setScore(numericScore);
	}
	
	public double getPointsEach() {
		return pointsEach;
	}
	
	public int getNumMissed() {
		return numMissed;
	}
}
