package Ch09_Lab06;

import Ch09_Lab04.GradedActivity;

public interface Analyzable {
	double getAverage();
	GradedActivity getHighest();
	GradedActivity getLowest();
}
