package Ch10_Lab01;

public class TestScores {
	private double[] scores;
	
	public TestScores(double[] scores) {
		try {
			for(int i = 0; i < scores.length; ++i) {
				if(scores[i] < 0)
					throw new IllegalArgumentException("less than 0");
				else if(scores[i] > 100)
					throw new IllegalArgumentException("greater than 100");
			}
			
			this.scores = new double[scores.length];
			
			for(int i = 0; i < scores.length; ++i) {
				this.scores[i] = scores[i];
			}
		}
		catch (IllegalArgumentException e){
			System.out.println("One of the scores is " + e.getMessage());
			
			throw new IllegalArgumentException();
		}
	}
	
	public double getAverage() {
		double total = 0;
		
		for(double i : this.scores) {
			total += i;
		}
		
		return total / this.scores.length;
	}
}
