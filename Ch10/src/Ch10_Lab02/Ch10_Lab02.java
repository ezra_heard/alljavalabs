package Ch10_Lab02;

import java.util.Scanner;

public class Ch10_Lab02 {

	public static void main(String[] args) {
		double[] scores;
		
		boolean error = false;
		
		TestScores testScores;
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.print("How many scores are you entering? ");
		
		scores = new double[userInput.nextInt()];
		
		
		
		do {
			System.out.println();
			
			for(int i = 0; i < scores.length; ++i) {
				System.out.print("Enter Score " + (i + 1) + ": ");
				scores[i] = userInput.nextDouble();
			}
			
			try {
				testScores = new TestScores(scores);
				
				System.out.printf("The average score is %.2f", testScores.getAverage());
				error = false;
			}
			catch(InvalidTestScore e) {
				userInput.nextLine(); // Clear the buffer
				
				System.out.println();
				
				System.out.println("Would you like to try again? (y/n)");
				String answer = userInput.nextLine();
				
				if(answer.toLowerCase().equals("y"))
					error = true;
				else
					error = false;
			}
			
		}while(error);
		
		userInput.close();
	}

}
