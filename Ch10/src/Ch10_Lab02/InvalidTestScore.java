package Ch10_Lab02;

public class InvalidTestScore extends Exception {

	private static final long serialVersionUID = -4604892139474309034L;

	public InvalidTestScore() {
		super("Error: Invalid Test Score Entered");
	}
	
	public InvalidTestScore(double score) {
		super("Error: Invalid Test Score Entered: " + score);
	}
}
