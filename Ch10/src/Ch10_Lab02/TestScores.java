package Ch10_Lab02;

public class TestScores {
	private double[] scores;
	
	public TestScores(double[] scores) throws InvalidTestScore{
		try {
			for(int i = 0; i < scores.length; ++i) {
				if(scores[i] < 0 || scores[i] > 100)
					throw new InvalidTestScore(scores[i]);
			}
			
			this.scores = new double[scores.length];
			
			for(int i = 0; i < scores.length; ++i) {
				this.scores[i] = scores[i];
			}
		}
		catch (InvalidTestScore e){
			System.out.println(e.getMessage());
			
			throw new InvalidTestScore();
		}
	}
	
	public double getAverage() {
		double total = 0;
		
		for(double i : this.scores) {
			total += i;
		}
		
		return total / this.scores.length;
	}
}
