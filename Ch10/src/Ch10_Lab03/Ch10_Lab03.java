package Ch10_Lab03;

public class Ch10_Lab03 {

	public static void main(String[] args) {
		RetailItem item1 = new RetailItem();
		RetailItem item2 = new RetailItem();
		RetailItem item3 = new RetailItem();
		RetailItem item4 = new RetailItem();
		
		//Test Jacket
		item1.setDescription("Jacket");
		try {
			item1.setUnitsOnHand(12);
			item1.setPrice(59.95);
			System.out.println(item1);
		}
		catch(NegativeUnits e){
			System.out.println(e.getMessage() + " for " + item1.getDescription());
		}
		catch(NegativePrice e){
			System.out.println(e.getMessage() + " for " + item1.getDescription());
		}
		
		System.out.println();
		
		//Test Jeans
		item2.setDescription("Jeans");
		try {
			item2.setUnitsOnHand(-20);
			item2.setPrice(59.95);
			System.out.println(item2);
		}
		catch(NegativeUnits e){
			System.out.println(e.getMessage() + " for " + item2.getDescription());
		}
		catch(NegativePrice e){
			System.out.println(e.getMessage() + " for " + item2.getDescription());
		}
		
		System.out.println();
		
		//Test Shirt
		item3.setDescription("Shirt");
		try {
			item3.setUnitsOnHand(12);
			item3.setPrice(-59.95);
			System.out.println(item3);
		}
		catch(NegativeUnits e){
			System.out.println(e.getMessage() + " for " + item3.getDescription());
		}
		catch(NegativePrice e){
			System.out.println(e.getMessage() + " for " + item3.getDescription());
		}
		
		System.out.println();
		
		//Test Pants
		item4.setDescription("Pants");
		try {
			item4.setUnitsOnHand(40);
			item4.setPrice(29.95);
			System.out.println(item4);
		}
		catch(NegativeUnits e){
			System.out.println(e.getMessage() + " for " + item4.getDescription());
		}
		catch(NegativePrice e){
			System.out.println(e.getMessage() + " for " + item4.getDescription());
		}
	}

}
