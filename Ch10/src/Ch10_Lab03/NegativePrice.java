package Ch10_Lab03;

public class NegativePrice extends Exception{
	
	private static final long serialVersionUID = 3336130677886262797L;

	public NegativePrice() {
		super("Error: Negative price entered");
	}
}
