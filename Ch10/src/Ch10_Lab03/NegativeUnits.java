package Ch10_Lab03;

public class NegativeUnits extends Exception{

	private static final long serialVersionUID = 6357163701854661104L;

	public NegativeUnits() {
		super("Error: Negative number of units entered");
	}
}
