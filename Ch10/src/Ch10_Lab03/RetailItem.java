package Ch10_Lab03;

public class RetailItem{
	private String description;
	private int unitsOnHand = 0;
	private double price = 0;
	
	public String getDescription() { return description; }
	
	public int getUnitsOnHand() { return unitsOnHand; }
	
	public double getPrice() { return price; }
	
	public void setDescription(String description) { this.description = description; }
	
	public void setUnitsOnHand(int units) throws NegativeUnits{ 
		if(units > 0)
			this.unitsOnHand = units; 
		else
			throw new NegativeUnits();
	}
	
	public void setPrice(double price) throws NegativePrice{
		if(price > 0)
			this.price = price; 
		else
			throw new NegativePrice();
	}
	
	@Override
	public String toString() {
		return "Name: " + this.description + "\nUnits: " + this.unitsOnHand + " | Price: " + this.price;
	}
}
