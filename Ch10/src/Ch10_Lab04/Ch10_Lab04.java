package Ch10_Lab04;

public class Ch10_Lab04 {

	public static void main(String[] args) {
		Month month = new Month();
		Month january = new Month(1);
		Month may = new Month("May");
		//Month error1 = new Month(0);
		//Month error2 = new Month("Poluary");
		
		System.out.println(month.getMonthNumber());
		System.out.println(january.getMonthName());
		System.out.println(may.toString());
		
		System.out.print("The month object and the january object are ");
		System.out.println(month.equals(january) ? "the same month" : "different months");
		
		System.out.print("The january object's month comes ");
		System.out.print(january.greaterThan(may) ? "after" : "before");
		System.out.println(" the may object's month");
		
		System.out.print("The may object's month comes ");
		System.out.print(may.lessThan(month) ? "before" : "after");
		System.out.println(" the month object's month");
	}

}
