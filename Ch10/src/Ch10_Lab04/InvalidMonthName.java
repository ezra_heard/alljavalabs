package Ch10_Lab04;

public class InvalidMonthName  extends Exception{
	private static final long serialVersionUID = -3010430528543008663L;

	public InvalidMonthName() {
		super("Invalid Month Name Entered");
	}
	
	public InvalidMonthName(String arg0) {
		super(arg0 + " is not a valid month name");
	}
}
