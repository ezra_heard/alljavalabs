package Ch10_Lab04;

public class InvalidMonthNumber extends Exception{
	private static final long serialVersionUID = 1719464243147936045L;

	public InvalidMonthNumber() {
		super("Invalid Month Number Entered");
	}
	
	public InvalidMonthNumber(String arg0) {
		super(arg0 + " is not a valid month number");
	}	
}
