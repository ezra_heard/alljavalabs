package Ch10_Lab04;

public class Month {
	private int monthNumber;
	
	public Month() {
		monthNumber = 1;
	}
	
	public Month(int month) {
		try {
			if(month < 1 || month > 12)
				throw new InvalidMonthNumber("" + month);
			
			monthNumber = month;
		}
		catch(InvalidMonthNumber e) {
			System.out.println(e.getMessage());
		}
	}
	
	public Month(String monthName) {
		monthName = monthName.toLowerCase();
		
		try {
			if(monthName.equals("january"))
				monthNumber = 1;
			else if(monthName.equals("february"))
				monthNumber = 2;
			else if(monthName.equals("march"))
				monthNumber = 3;
			else if(monthName.equals("april"))
				monthNumber = 4;
			else if(monthName.equals("may"))
				monthNumber = 5;
			else if(monthName.equals("june"))
				monthNumber = 6;
			else if(monthName.equals("july"))
				monthNumber = 7;
			else if(monthName.equals("august"))
				monthNumber = 8;
			else if(monthName.equals("september"))
				monthNumber = 9;
			else if(monthName.equals("october"))
				monthNumber = 10;
			else if(monthName.equals("november"))
				monthNumber = 11;
			else if(monthName.equals("december"))
				monthNumber = 12;
			else
				throw new InvalidMonthName(monthName);
		}
		catch(InvalidMonthName e){
			System.out.println(e.getMessage());
		}
	}

	public int getMonthNumber() {
		return monthNumber;
	}
	
	public String getMonthName() {
		switch(monthNumber) {
		case 1:
			return "January";
		case 2:
			return "February";
		case 3:
			return "March";
		case 4:
			return "April";
		case 5:
			return "May";
		case 6:
			return "June";
		case 7:
			return "July";
		case 8:
			return "August";
		case 9:
			return "September";
		case 10:
			return "October";
		case 11:
			return "November";
		case 12:
			return "December";
		default:
			return "Error!!!";
		}
	}

	public String toString() {
		return getMonthName();
	}
	
	public boolean equals(Month month) {
		if(this.monthNumber == month.monthNumber)
			return true;
		else
			return false;
	}
	
	public boolean greaterThan(Month month) {
		if(this.monthNumber > month.monthNumber)
			return true;
		else
			return false;
	}
	
	boolean lessThan(Month month) {
		if(this.monthNumber < month.monthNumber)
			return true;
		else
			return false;
	}
}
