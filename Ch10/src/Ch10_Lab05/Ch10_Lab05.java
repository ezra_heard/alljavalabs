package Ch10_Lab05;

import java.util.Scanner;

public class Ch10_Lab05 {

	public static void main(String[] args) {
		boolean error = false;
		
		String name = null, id = null;
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.print("Enter employees Name: ");
		try {
			name = userInput.nextLine();
			
			if(name.equals("") || name == "" || name.equals(" ") || name == " " || name == null) {
				userInput.close();
				throw new EmptyName();
			}
		}
		catch(EmptyName e) {
			System.out.println(e.getMessage());
			error = true;
		}
		
		if(!error) {
			System.out.print("Enter employees ID Number: ");
			try {
				id = userInput.nextLine();
				
				if(id.equals("") || id == "" || id.equals(" ") || id == " " || id == null) {
					userInput.close();
					throw new EmptyIdNumber();
				}
			}
		
			catch(EmptyIdNumber e) {
				System.out.println(e.getMessage());
				error = true;
			}
		}
		
		if(!error) {
			Payroll employee1 = new Payroll(name, id);
			
			System.out.print("Enter the employees hourly pay: ");
			try {
				double pay = userInput.nextDouble();
				
				if(pay < 0) {
					userInput.close();
					throw new InvalidPayRate("negative payrate");
				}
				else if(pay > 25) {
					userInput.close();
					throw new InvalidPayRate("over $25.00/hr");
				}
				
				employee1.setHourlyPay(pay);
			}
			catch(InvalidPayRate e) {
				System.out.println(e.getMessage());
				error = true;
			}
			
			if(!error) {
				System.out.print("Enter the employees hours worked: ");
				try {
					double hoursWorked = userInput.nextDouble();
					
					if(hoursWorked < 0) {
						userInput.close();
						throw new InvalidHoursWorked("negative hours");
					}
					else if(hoursWorked > 84) {
						userInput.close();
						throw new InvalidHoursWorked("over 84hr");
					}
						
					employee1.setHoursWorked(hoursWorked);
				}
				catch(InvalidHoursWorked e) {
					System.out.println(e.getMessage());
					error = true;
				}
			}
			
			if(!error)
				System.out.printf("The employees gross pay is: $%,.2f", employee1.calcGrossPay());
		}
		
		userInput.close();
	}

}
