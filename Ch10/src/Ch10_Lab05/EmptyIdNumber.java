package Ch10_Lab05;

public class EmptyIdNumber extends Exception {

	private static final long serialVersionUID = 9065802204031766945L;

	public EmptyIdNumber() {
		super("Employee ID cannot be empty");
	}
	
	public EmptyIdNumber(String arg0) {
		super(arg0);
	}
}
