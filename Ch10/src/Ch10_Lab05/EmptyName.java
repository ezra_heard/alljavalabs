package Ch10_Lab05;

public class EmptyName extends Exception {

	private static final long serialVersionUID = 1394986012783136986L;

	public EmptyName() {
		super("Employee Name cannot be empty");
	}
	
	public EmptyName(String arg0) {
		super(arg0);
	}
}
