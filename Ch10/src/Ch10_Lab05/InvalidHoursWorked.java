package Ch10_Lab05;

public class InvalidHoursWorked extends Exception {

	private static final long serialVersionUID = -1088885656598863427L;

	public InvalidHoursWorked() {
		super("Invalid number of hours worked");
	}
	
	public InvalidHoursWorked(String arg0) {
		super("Invalid number of hours worked: " + arg0);
	}
}
