package Ch10_Lab05;

public class InvalidPayRate extends Exception {

	private static final long serialVersionUID = -6808545702317043062L;

	public InvalidPayRate() {
		super("Invalid amount for payrate");
	}
	
	public InvalidPayRate(String arg0) {
		super("Invalid amount for payrate: " + arg0);
	}
}
