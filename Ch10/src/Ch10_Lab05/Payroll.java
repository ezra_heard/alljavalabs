package Ch10_Lab05;

public class Payroll {
	private String name;
	private String idNumber;
	private double hourlyPay;
	private double hoursWorked;
	
	public Payroll(String name, String idNumber) {
		this.name = name;
		this.idNumber = idNumber;
	}
	
	public void setName(String name) { this.name = name; }
	public String getName() { return name; }
	
	public void setIdNumber(String idNumber) { this.idNumber = idNumber; }
	public String getIdNumber() { return idNumber; }
	
	public void setHourlyPay(double hourlyPay) { this.hourlyPay = hourlyPay; }
	public double getHourlyPay() { return hourlyPay; }
	
	public void setHoursWorked(double hoursWorked) { this.hoursWorked = hoursWorked; }
	public double getHoursWorked() { return hoursWorked; }
	
	public double calcGrossPay() {
		return hourlyPay * hoursWorked;
	}
}
