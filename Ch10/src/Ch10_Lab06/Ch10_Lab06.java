package Ch10_Lab06;

import java.io.*;

public class Ch10_Lab06 {
	public static void main(String[] args) {
		int[] numbers = { 2, 4, 6, 8, 10, 12, 14 };
		int[] copyNumbers = null;
		
		
		try {
			FileArray.writeArray("src/Ch10_Lab06/Numbers.dat", numbers);
		}
		catch(IOException e) {
			System.out.println(e.getMessage());
		}
		try {
			copyNumbers = FileArray.readArray("src/Ch10_Lab06/Numbers.dat", copyNumbers);
			
			for(int i : copyNumbers) {
				System.out.println(i);
			}
		}
		catch(IOException e) {
			System.out.println(e.getMessage());
		}
	}
}
