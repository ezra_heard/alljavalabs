package Ch10_Lab06;

import java.io.*;

public class FileArray {
	public static void writeArray(String fileName, int[] numbers) throws IOException{
		FileOutputStream fstream = new FileOutputStream(fileName);
		DataOutputStream outputFile = new DataOutputStream(fstream);
		
		for(int i : numbers) {
			outputFile.writeInt(i);
		}
	
		outputFile.close();
	}
	
	public static int[] readArray(String fileName, int[] numbers) throws IOException{
		FileInputStream fstream = new FileInputStream(fileName);
		DataInputStream inputFile = new DataInputStream(fstream);
		
		int[] temp = new int[1000];
		
		int i = 0;
		
		for(boolean sentinal = true;  sentinal; ++i) {
			try {
				int tempInt = inputFile.readInt();
				temp[i] = tempInt;
			}
			catch(EOFException e) {
				sentinal = false;
				--i;
			}
		}
		
		numbers = new int[i];
		
		for(int x = 0; x < i; ++x) {
			numbers[x] = temp[x];
		}
		
		inputFile.close();
		return numbers;
	}
}
