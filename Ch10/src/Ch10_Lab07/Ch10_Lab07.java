package Ch10_Lab07;

import java.io.*;

public class Ch10_Lab07 {

	public static void main(String[] args) {
		//For writing initial file information, can be used to change the text in the baseData file.
		//DataOutputStream outputFile = new DataOutputStream(new FileOutputStream("src/Ch10_Lab07/baseData.dat"));
		//outputFile.writeChars("Only two things are infinite, the universe and human stupidity, and I'm not sure about the former.");
		
		//Test for reading encrypted information without decrypting it
		/*
		DataInputStream inputFile = new DataInputStream(new FileInputStream("src/Ch10_Lab07/encrptedData.dat"));
		
		for(boolean sentinal = true; sentinal; ) {
			try {
				System.out.print(inputFile.readChar());
			}
			catch(EOFException e) {
				sentinal = false;
			}
		}
		
		inputFile.close();
		*/
		
		//Open stream to both files
		//Read from an unencrypted file
		//Encrypt each char
		//Write contents to the encrypt file
		DataInputStream inputFile = null;
		DataOutputStream outputFile = null;
		
		try {
			inputFile = new DataInputStream(new FileInputStream("src/Ch10_Lab07/baseData.dat"));
			outputFile = new DataOutputStream(new FileOutputStream("src/Ch10_Lab07/encrptedData.dat"));
		}
		catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

		if(outputFile != null && inputFile != null) {
			int i = 1;
			
			for(boolean sentinal = true; sentinal; ++i) {
				try {
					int encryptedChar = inputFile.readChar() + (i % 2 == 0 ? 2 : 1);
					outputFile.writeChar(encryptedChar);
				}
				catch(EOFException e) {
					sentinal = false;
				}
				catch (IOException e) {
					System.out.println("Critical Error occured");
				}
			}
			
			System.out.println("File sucessfully encrypted.");
		}
	}

}
