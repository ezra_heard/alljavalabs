package Ch10_Lab08;

import java.io.*;

public class Ch10_Lab08 {

	public static void main(String[] args) {
		//Test for reading decrypted data
		/*
		DataInputStream inputFile = new DataInputStream(new FileInputStream("src/Ch10_Lab08/decrptedData.dat"));
		
		for(boolean sentinal = true; sentinal; ) {
			try {
				System.out.print(inputFile.readChar());
			}
			catch(EOFException e) {
				sentinal = false;
			}
		}
		
		inputFile.close();
		*/
		
		DataInputStream inputFile = null;
		DataOutputStream outputFile = null;
		
		try {
			inputFile = new DataInputStream(new FileInputStream("src/Ch10_Lab07/encrptedData.dat"));
			outputFile = new DataOutputStream(new FileOutputStream("src/Ch10_Lab08/decrptedData.dat"));
		}
		catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

		if(outputFile != null && inputFile != null) {
			int i = 1;
			
			for(boolean sentinal = true; sentinal; ++i) {
				try {
					int decryptedChar = inputFile.readChar() - (i % 2 == 0 ? 2 : 1);
					outputFile.writeChar(decryptedChar);
				}
				catch(EOFException e) {
					sentinal = false;
				}
				catch (IOException e) {
					System.out.println("Critical Error occured");
				}
			}
		
			System.out.println("File sucessfully decrypted.");
		}
		
	}

}
