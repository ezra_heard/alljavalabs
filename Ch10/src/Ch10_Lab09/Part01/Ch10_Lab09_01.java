package Ch10_Lab09.Part01;

import Ch10_Lab09.TestScores;
import java.io.*;

public class Ch10_Lab09_01 {

	public static void main(String[] args) throws IOException{
		TestScores scores = new TestScores(new double[] {99, 88, 77, 66, 55});
		
		ObjectOutputStream objectOutputFile;
		
		objectOutputFile = new ObjectOutputStream(new FileOutputStream("src/Ch10_Lab09/TestScores.dat"));
		
		objectOutputFile.writeObject(scores);
		
		System.out.println("Scores object successfully saved");
		
		objectOutputFile.close();
	}

}
