package Ch10_Lab09.Part02;

import Ch10_Lab09.TestScores;
import java.io.*;

public class Ch10_Lab09_02 {

	public static void main(String[] args) throws IOException, ClassNotFoundException{
		TestScores scores;
		
		ObjectInputStream objectInputFile;
		
		objectInputFile = new ObjectInputStream(new FileInputStream("src/Ch10_Lab09/TestScores.dat"));
		
		scores = (TestScores)objectInputFile.readObject();
		
		System.out.println("Average Score: " + scores.getAverage());
		
		objectInputFile.close();
	}

}
