package Ch10_Lab10;

public class BankAccount {
	private double balance;   // Account balance
	private double interestRate; // Interest rate
	private double interest;   // Interest earned
	
	public BankAccount(double startBalance, double intRate) throws NegativeStartingBalance, NegativeInterestRate{
		if(startBalance < 0)
			throw new NegativeStartingBalance(startBalance);
		if(intRate < 0)
			throw new NegativeInterestRate(intRate);
		
		balance = startBalance;
		interestRate = intRate;
		interest = 0.0;
	}
	
	public void deposit(double amount) {
		balance += amount;
	}
	
	public void withdraw(double amount) {
		balance -= amount;
	}
	
	public void addInterest() {
		interest = balance * interestRate;
		balance += interest;
	}
	
	public double getBalance() { return balance; }
	public double getInterestRate() { return interestRate; }
	public double getInterest() { return interest; }
}
