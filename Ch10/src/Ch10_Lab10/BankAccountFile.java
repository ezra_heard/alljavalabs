package Ch10_Lab10;

import java.io.*;

public class BankAccountFile {
	private final long RECORD_SIZE = 16;
	private RandomAccessFile ras;
	
	public BankAccountFile(String fileName) throws FileNotFoundException{
		ras = new RandomAccessFile(fileName, "rw");
	}
	
	public void writeBankAccount(BankAccount ba) throws IOException {
		//Write BankAccount Object Here
		ras.writeDouble(ba.getBalance());
		ras.writeDouble(ba.getInterestRate());
	}
	
	public BankAccount readBankAccount() throws IOException, NegativeStartingBalance, NegativeInterestRate {
		//Read BankAccount Object Here
		double balance;   // Account balance
		double interestRate; // Interest rate
		
		balance = ras.readDouble();
		interestRate = ras.readDouble();
		
		return new BankAccount(balance, interestRate);
	}
	
	public void movePointer(long recordNum) throws IOException {
		//If the byte isn't found throw the exception up a level
		ras.seek(getByteNum(recordNum));;
	}
	
	private long getByteNum(long recordNum) {
		return RECORD_SIZE * recordNum;
	}
	
	public void close() throws IOException {
		ras.close();
	};
}
