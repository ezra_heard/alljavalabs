package Ch10_Lab10;

import java.io.*;
import java.text.DecimalFormat;

public class Ch10_Lab10 {

	public static void main(String[] args) throws IOException, NegativeStartingBalance, NegativeInterestRate {
		DecimalFormat formatter = new DecimalFormat("0.00");
		BankAccountFile baf = new BankAccountFile("src/Ch10_Lab10/BankAccounts.dat");
		BankAccount account;
		
		//create records
		baf.writeBankAccount(new BankAccount(10000, 0.01));
		baf.writeBankAccount(new BankAccount(50000, 0.02));
		baf.writeBankAccount(new BankAccount(2000, 0.05));
		
		//randomly look at records
		baf.movePointer(1);
		account = baf.readBankAccount();
		
		System.out.println("Account 2:\n\tBalance: $" + formatter.format(account.getBalance()) + "\n\tInterest Rate: " + formatter.format(account.getInterestRate()));
		System.out.println();
		
		//randomly modify records
		baf.movePointer(1);
		baf.writeBankAccount(new BankAccount(9999, 9.99));
		
		baf.movePointer(1);
		account = baf.readBankAccount();
		System.out.println("Account 2:\n\tBalance: $" + formatter.format(account.getBalance()) + "\n\tInterest Rate: " + formatter.format(account.getInterestRate()));
		
		account = baf.readBankAccount();
		System.out.println("Account 3:\n\tBalance: $" + formatter.format(account.getBalance()) + "\n\tInterest Rate: " + formatter.format(account.getInterestRate()));
	
		baf.close();
	}
}
