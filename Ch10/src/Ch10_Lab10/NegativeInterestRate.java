package Ch10_Lab10;

public class NegativeInterestRate extends Exception {
	private static final long serialVersionUID = 5519531324899708059L;

	public NegativeInterestRate(){
		super("Error: Negative interest rate");
	}
	
	public NegativeInterestRate(double amount){
		super("Error: Negative interest rate: " + amount);
	}
}
