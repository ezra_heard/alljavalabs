package Ch10_Lab11;

import java.util.Scanner;

public class Ch10_Lab11 {

	public static void main(String[] args) {
		ProductionWorker[] employees = new ProductionWorker[3];
		
		Scanner userInput = new Scanner(System.in);
		
		for(int i = 0; i < employees.length; ++i) {
			boolean invalid = false; //Used for error checking
			
			employees[i] = new ProductionWorker();
			
			System.out.print("Enter Employee " + (i + 1) + "'s Name: ");
			employees[i].setName(userInput.nextLine());
			
			do{
				System.out.print("Enter Employee " + (i + 1) + "'s ID Number: ");
				
				try {
					employees[i].setEmployeeNumber(userInput.nextInt());
					invalid = false;
				}
				catch (InvalidEmployeeNumber e) {
					System.out.println(e.getMessage());
					invalid = true;
				}
			}while(invalid); 
			
			userInput.nextLine(); //Clear Buffer
			
			System.out.print("Enter Employee " + (i + 1) + "'s Hire Date: ");
			employees[i].setHireDate(userInput.nextLine());
			
			do{
				System.out.print("Enter Employee " + (i + 1) + "'s Shift (1 = day, 2 = night): ");
				
				try {
					employees[i].setShift(userInput.nextInt());
					invalid = false;
				}
				catch (InvalidShift e) {
					System.out.println(e.getMessage());
					invalid = true;
				}
			}while(invalid);
			
			do{
				System.out.print("Enter Employee " + (i + 1) + "'s Payrate: ");
				
				try {
					employees[i].setHourlyPay(userInput.nextDouble());
					invalid = false;
				}
				catch (InvalidPayRate e) {
					System.out.println(e.getMessage());
					invalid = true;
				}
			}while(invalid);
			
			userInput.nextLine(); //Clear Buffer
			System.out.println();
		}
		
		for(ProductionWorker worker : employees) {
			System.out.println();
			System.out.println(worker.toString());	
		}
		
		userInput.close();
	}

}
