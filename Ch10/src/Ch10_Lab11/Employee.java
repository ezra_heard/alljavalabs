package Ch10_Lab11;

public class Employee {
	private String name;
	private int employeeNumber;
	private String hireDate;
	
	public Employee() {}

	public Employee(String name, int employeeNumber, String hireDate) throws InvalidEmployeeNumber {
		this.name = name;
		this.hireDate = hireDate;
		
		try {
			if(employeeNumber >= 0 && employeeNumber <= 9999)
				this.employeeNumber = employeeNumber;
			else if(employeeNumber < 0)
				throw new InvalidEmployeeNumber("cannot be negative");
			else
				throw new InvalidEmployeeNumber("cannot be over 9999");
		}
		catch(InvalidEmployeeNumber e) {
			throw e;
		}
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setEmployeeNumber(int employeeNumber) throws InvalidEmployeeNumber {
		try {
			if(employeeNumber >= 0 && employeeNumber <= 9999)
				this.employeeNumber = employeeNumber;
			else if(employeeNumber < 0)
				throw new InvalidEmployeeNumber("cannot be negative");
			else
				throw new InvalidEmployeeNumber("cannot be over 9999");
		}
		catch(InvalidEmployeeNumber e) {
			throw e;
		}
	}

	public void setHireDate(String hireDate) {
		this.hireDate = hireDate;
	}
	
	public String getName() {
		return name;
	}

	public int getEmployeeNumber() {
		return employeeNumber;
	}

	public String getHireDate() {
		return hireDate;
	}
	
	public String toString() {
		return "Employee Name: " + this.name + "\nEmployee Number: " + this.employeeNumber + "\nHire Date: " + this.hireDate;
	}
}
