package Ch10_Lab11;

public class InvalidEmployeeNumber extends Exception{

	private static final long serialVersionUID = 1386723627090165144L;

	public InvalidEmployeeNumber() {
		super("Invalid Employee Number");
	}
	
	public InvalidEmployeeNumber(String arg0) {
		super("Invalid Employee Number: " + arg0);
	}
}
