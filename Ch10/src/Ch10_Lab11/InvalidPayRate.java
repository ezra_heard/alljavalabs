package Ch10_Lab11;

public class InvalidPayRate extends Exception {

	private static final long serialVersionUID = -6245651606672517325L;

	public InvalidPayRate() {
		super("Invalid Employee Number");
	}
	
	public InvalidPayRate(String arg0) {
		super("Invalid Employee Number: " + arg0);
	}
}
