package Ch10_Lab11;

public class InvalidShift extends Exception {

	private static final long serialVersionUID = -7930186680055795193L;

	public InvalidShift() {
		super("Invalid Shift Number");
	}
	
	public InvalidShift(String arg0) {
		super("Invalid Shift Number: " + arg0);
	}
}
