package Ch10_Lab11;

public class ProductionWorker extends Employee {
	private int shift;
	private double hourlyPay;
	
	public final int DAY_SHIFT = 1;
	public final int NIGHT_SHIFT = 2;
	
	public ProductionWorker() {}
	
	public ProductionWorker(int shift, double hourlyPay) throws InvalidShift, InvalidPayRate{
		super();
		
		try {
			if(shift == DAY_SHIFT || shift == NIGHT_SHIFT)
				this.shift = shift;
			else
				throw new InvalidShift("can only be 1 or 2");
			
			if(hourlyPay > 0)
				this.hourlyPay = hourlyPay;
			else
				throw new InvalidPayRate("cannot be negative");
		}
		catch(InvalidShift e) {
			throw e;
		}
		catch(InvalidPayRate e) {
			throw e;
		}
	}
	
	public ProductionWorker(String name, int employeeNumber, String hireDate, int shift, double hourlyPay) throws InvalidEmployeeNumber, InvalidShift, InvalidPayRate {
		super(name, employeeNumber, hireDate);
		
		try {
			if(shift == DAY_SHIFT || shift == NIGHT_SHIFT)
				this.shift = shift;
			else
				throw new InvalidShift("can only be 1 or 2");
			
			if(hourlyPay > 0)
				this.hourlyPay = hourlyPay;
			else
				throw new InvalidPayRate("cannot be negative");
		}
		catch(InvalidShift e) {
			throw e;
		}
		catch(InvalidPayRate e) {
			throw e;
		}
	}

	public int getShift() {
		return shift;
	}

	public double getHourlyPay() {
		return hourlyPay;
	}

	public void setShift(int shift) throws InvalidShift {
		try {
			if(shift == DAY_SHIFT || shift == NIGHT_SHIFT)
				this.shift = shift;
			else
				throw new InvalidShift("can only be 1 or 2");
		}
		catch(InvalidShift e) {
			throw e;
		}
	}

	public void setHourlyPay(double hourlyPay) throws InvalidPayRate {
		try {
			if(hourlyPay > 0)
				this.hourlyPay = hourlyPay;
			else
				throw new InvalidPayRate("cannot be negative");
		}
		catch(InvalidPayRate e) {
			throw e;
		}
	}
	
	@Override
	public String toString() {
		String shiftString;
		
		if(shift == DAY_SHIFT)
			shiftString = "Day";
		else if(shift == NIGHT_SHIFT)
			shiftString = "Night";
		else
			shiftString = "invalid";
			
		return super.toString() + "\nShift: " + shiftString + " shift\nPay Rate: $" + String.format("%,.2f", this.hourlyPay); 
	}
}
