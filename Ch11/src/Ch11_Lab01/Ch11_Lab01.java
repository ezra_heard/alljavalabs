package Ch11_Lab01;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.geometry.Pos;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class Ch11_Lab01 extends Application{

	private Label displayLabel;
	
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage mainStage) throws Exception {
		//Work with Buttons
		Button leftButton = new Button("sinister");
		Button centerButton = new Button("dexter");
		Button rightButton = new Button("medium");
		
		leftButton.setAlignment(Pos.TOP_LEFT);
		centerButton.setAlignment(Pos.TOP_CENTER);
		rightButton.setAlignment(Pos.TOP_RIGHT);
		
		leftButton.setOnAction(new ButtonClickHandler());
		centerButton.setOnAction(new ButtonClickHandler());
		rightButton.setOnAction(new ButtonClickHandler());
		
		//Work with Labels
		displayLabel = new Label("");
		displayLabel.setAlignment(Pos.CENTER);
		
		//Work with HBox
		HBox buttonsBox = new HBox(10, leftButton, centerButton, rightButton);
		buttonsBox.setAlignment(Pos.CENTER);
		
		//Work with VBox
		VBox vbox = new VBox(15, buttonsBox, displayLabel);
		vbox.setAlignment(Pos.CENTER);
		
		//Work with Scene
		Scene scene = new Scene(vbox, 300, 200);
		
		//Work with Stage
		mainStage.setTitle("Latin to English converter");
		
		mainStage.setScene(scene);
		
		// Show the window.
		mainStage.show();
		
	}

	private class ButtonClickHandler implements EventHandler<ActionEvent>
	{
	  @Override
	  public void handle(ActionEvent event)
	  {
		  Button thisButton = (Button)event.getSource();
		  
		  if(thisButton.getText() == "sinister")
			  displayLabel.setText("Left");
		  else if(thisButton.getText() == "dexter")
			  displayLabel.setText("Right");
		  else if(thisButton.getText() == "medium")
			  displayLabel.setText("Center");
		  else
			  displayLabel.setText("");
	  }
	}
}
