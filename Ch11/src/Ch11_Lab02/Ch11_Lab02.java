package Ch11_Lab02;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.geometry.Insets;
import javafx.geometry.Pos;

public class Ch11_Lab02 extends Application{

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage mainStage) throws Exception {
		
		//Work with Labels
		Label firstNameLabel = new Label("First Name:  ");
		Label middleNameLabel = new Label("Middle Name:  ");
		Label lastNameLabel = new Label("Last Name:  ");
		Label titleLabel = new Label("Preferred Title:  ");
		Label resultsLabel = new Label("\r\n" + "\r\n" + "\r\n" + "\r\n" + "\r\n");
		
		//Work with TextFields
		TextField firstNameTextField = new TextField();
		TextField middleNameTextField = new TextField();
		TextField lastNameTextField = new TextField();
		TextField titleTextField = new TextField();
		
		//Work with Buttons
		Button formatNamesButton = new Button("Format Names");
		formatNamesButton.setOnAction(event -> {
			String firstName = firstNameTextField.getText();
			String middleName = middleNameTextField.getText();
			String lastName = lastNameTextField.getText();
			String title = titleTextField.getText();
			
			if(firstName.equals("") || middleName.equals("") || lastName.equals("") || title.equals(""))
				resultsLabel.setText("\r\n" + "\r\n" + "\r\n" + "\r\n" + "\r\n");
			else
				resultsLabel.setText(title + " " + firstName + " " + middleName + " " + lastName + "\r\n" +
						firstName + " " + middleName + " " + lastName + "\r\n" +
						firstName + " " + lastName + "\r\n" +
						lastName + ", " + firstName + " " + middleName + ", " + title + "\r\n" +
						lastName + ", " + firstName + " " + middleName + "\r\n" +
						lastName + ", " + firstName);
		});
		
		//Work with VBox
		VBox nameLabelsVBox = new VBox(15, firstNameLabel, middleNameLabel, lastNameLabel, titleLabel);
		VBox nameTextFieldsVBox = new VBox(6, firstNameTextField, middleNameTextField, lastNameTextField, titleTextField);
		VBox resultsVBox = new VBox(10, formatNamesButton, resultsLabel);
		
		nameLabelsVBox.setAlignment(Pos.TOP_RIGHT);
		nameTextFieldsVBox.setAlignment(Pos.TOP_LEFT);
		resultsVBox.setAlignment(Pos.TOP_CENTER);
		
		//Work with BorderPane's
		BorderPane borderpane = new BorderPane(nameLabelsVBox, null, nameTextFieldsVBox, resultsVBox, null);

		BorderPane.setMargin(nameLabelsVBox, new Insets(50, 0, 0, 0));
		BorderPane.setMargin(nameTextFieldsVBox, new Insets(50, 80, 0, 0));
		BorderPane.setMargin(resultsVBox, new Insets(0, 0, 50, 0));
		
		//Work with Scene
		Scene scene = new Scene(borderpane, 400, 370);
		
		//Work with Stage
		mainStage.setTitle("Name Formatter");
		
		mainStage.setScene(scene);
		
		// Show the window.
		mainStage.show();
		
	}

}
