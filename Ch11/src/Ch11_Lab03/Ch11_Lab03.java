package Ch11_Lab03;

import java.text.DecimalFormat;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.geometry.Pos;

public class Ch11_Lab03 extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage mainStage) throws Exception {
		//Work with Labels
		Label costLabel = new Label("Meal Cost:  ");
		Label resultsLabel = new Label("\r\n\r\n\r\n");
		
		//Work with TextFields
		TextField costTextField = new TextField();
		
		//Work with Buttons
		Button calcTotals = new Button("Calculate Tax, Tip, and Total");
		
		calcTotals.setOnAction(event -> {
			double cost = 0, tip, tax, total;
			
			boolean validCost = true;
			
			DecimalFormat formatter = new DecimalFormat("$#.00");
			
			try {
				cost = Double.parseDouble(costTextField.getText());
			}
			catch(Exception e){
				validCost = false;
			}
			
			if(validCost) {
				tip = 0.18 * cost;
				tax = 0.07 * cost;
				total = cost + tip + tax;
				
				resultsLabel.setText("Food Price: " + formatter.format(cost) + "\r\n" +
									 "Tip: " + formatter.format(tip) + "\r\n" +
									 "Tax: " + formatter.format(tax) + "\r\n" +
									 "Total: " + formatter.format(total)
				);
			}
			else
				resultsLabel.setText("\r\n\r\n\r\n");
			
		});
		
		//Work with HBox
		HBox hbox = new HBox(10, costLabel, costTextField);
		hbox.setAlignment(Pos.CENTER);
		
		//Work with VBox
		VBox vbox = new VBox(15, hbox, calcTotals, resultsLabel);
	
		vbox.setAlignment(Pos.CENTER);
		
		//Work with Scene
		Scene scene = new Scene(vbox, 300, 250);
		
		//Work with Stage
		mainStage.setTitle("Tip, Tax, and Total");
		
		mainStage.setScene(scene);
		
		// Show the window.
		mainStage.show();
	}
}
