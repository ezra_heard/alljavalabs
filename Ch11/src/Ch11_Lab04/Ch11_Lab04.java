package Ch11_Lab04;

import java.text.DecimalFormat;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.geometry.Pos;


public class Ch11_Lab04 extends Application{

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage mainStage) throws Exception {
		//Work with Labels
		Label actualValueLabel = new Label("Property Value:  ");
		Label resultsLabel = new Label("\r\n\r\n");
		
		//Work with TextFields
		TextField actualValueTextField = new TextField();
		
		//Work with Buttons
		Button calcTotals = new Button("Calculate Assessment and Tax");
		
		calcTotals.setOnAction(event -> {
			double value = 0, assessedValue, tax;
			
			boolean validValue = true;
			
			DecimalFormat formatter = new DecimalFormat("$0.00");
			
			try {
				value = Double.parseDouble(actualValueTextField.getText());
			}
			catch(Exception e){
				validValue = false;
			}
			
			if(validValue) {
				assessedValue = value * 0.60;
				tax = 0.64 * (int)(assessedValue / 100);
				
				resultsLabel.setText("Actual Value: " + formatter.format(value) + "\r\n" +
									 "Assessed Value: " + formatter.format(assessedValue) + "\r\n" +
									 "Property Tax: " + formatter.format(tax));
			}
			else
				resultsLabel.setText("\r\n\r\n");
		});
		
		//Work with HBox
		HBox hbox = new HBox(10, actualValueLabel, actualValueTextField);
		hbox.setAlignment(Pos.CENTER);
		
		//Work with VBox
		VBox vbox = new VBox(15, hbox, calcTotals, resultsLabel);
	
		vbox.setAlignment(Pos.CENTER);
		
		//Work with Scene
		Scene scene = new Scene(vbox, 300, 250);
		
		//Work with Stage
		mainStage.setTitle("Property Tax");
		
		mainStage.setScene(scene);
		
		// Show the window.
		mainStage.show();
	}

}
