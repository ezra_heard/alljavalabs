package Ch11_Lab05;

import java.util.Random;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.geometry.Insets;
import javafx.geometry.Pos;

public class Ch11_Lab05 extends Application{

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage mainStage) throws Exception {
		
		//Work with Images
		Image headsImage = new Image("file:src/Ch11_Lab05/coin_heads.png");
		Image tailsImage = new Image("file:src/Ch11_Lab05/coin_tails.png");
		ImageView coinImageView = new ImageView();
		
		//Work with button
		Button flipCoinButton = new Button("Flip Coin");
		flipCoinButton.setOnAction( e -> {
			Random rng = new Random();
			
			int headTails = rng.nextInt(2);
			
			if(headTails == 0)
				coinImageView.setImage(headsImage);
			else if(headTails == 1)
				coinImageView.setImage(tailsImage);
		});
		
		//Work with BorderPane
		BorderPane borderPane = new BorderPane(coinImageView, null, null, flipCoinButton, null);
		BorderPane.setAlignment(flipCoinButton, Pos.CENTER);
		BorderPane.setMargin(flipCoinButton, new Insets(0, 0, 20, 0));
		
		//Work with Scene
		Scene scene = new Scene(borderPane, 500, 500);
		
		//Work with Stage
		mainStage.setTitle("Property Tax");
		
		mainStage.setScene(scene);
		
		// Show the window.
		mainStage.show();
	}

}
