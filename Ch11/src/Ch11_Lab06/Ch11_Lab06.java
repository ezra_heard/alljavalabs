package Ch11_Lab06;

import java.util.Random;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.geometry.Insets;
import javafx.geometry.Pos;

public class Ch11_Lab06 extends Application{

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage mainStage) throws Exception {
		//Work with Images
		ImageView die1ImageView = new ImageView();
		ImageView die2ImageView = new ImageView();
		
		//Work with button
		Button rollDiceButton = new Button("Roll Dice");
		rollDiceButton.setOnAction( e -> {
			Random rng = new Random();
			
			int die1 = rng.nextInt(6) + 1;
			int die2 = rng.nextInt(6) + 1;
			
			die1ImageView.setImage(new Image("file:src/Ch11_Lab06/dice_side" + die1 + ".png"));
			die2ImageView.setImage(new Image("file:src/Ch11_Lab06/dice_side" + die2 + ".png"));
		});
		
		//Work with HBox
		HBox diceBox = new HBox(30, die1ImageView, die2ImageView);
		HBox.setMargin(die1ImageView, new Insets(110, 0, 0, 90));
		HBox.setMargin(die2ImageView, new Insets(110, 0, 0, 50));
		
		//Work with BorderPane
		BorderPane borderPane = new BorderPane(diceBox, null, null, rollDiceButton, null);
		BorderPane.setAlignment(diceBox, Pos.CENTER);
		BorderPane.setAlignment(rollDiceButton, Pos.CENTER);
		BorderPane.setMargin(rollDiceButton, new Insets(0, 0, 20, 0));
		
		//Work with Scene
		Scene scene = new Scene(borderPane, 500, 350);
		
		//Work with Stage
		mainStage.setTitle("Property Tax");
		
		mainStage.setScene(scene);
		
		// Show the window.
		mainStage.show();
	}

}
