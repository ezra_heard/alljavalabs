package Ch11_Lab07;

import java.text.DecimalFormat;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.geometry.Insets;
import javafx.geometry.Pos;

public class Ch11_Lab07 extends Application{

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage mainStage) throws Exception {
		//Work with Labels
		Label daysOnTripLabel = new Label("Days On Trip:  ");
		Label airfareLabel = new Label("Airfare Fee:  ");
		Label carRentalLabel = new Label("Car Rental Fee:  ");
		Label milesDrivenLabel = new Label("Miles Driven (personally):  ");
		Label parkingLabel = new Label("Parking Fee:  ");
		Label taxiChargesLabel = new Label("Taxi Charges:  ");
		Label registrationLabel = new Label("Conference/Seminar Registration Fee:  ");
		Label lodgingLabel = new Label("Loding Charges (per night):  ");
		
		
		//Work with results labels
		Label resultsLabel = new Label("\r\n\r\n");
		
		
		//Work with TextFields
		TextField daysOnTripTextField = new TextField();
		TextField airfareTextField = new TextField();
		TextField carRentalTextField = new TextField();
		TextField milesDrivenTextField = new TextField();
		TextField parkingTextField = new TextField();
		TextField taxiChargesTextField = new TextField();
		TextField registrationTextField = new TextField();
		TextField lodgingTextField = new TextField();
		
		//Work with Buttons
		Button calculateExpensesButton = new Button("Calculate Expenses");
		calculateExpensesButton.setOnAction(event -> {
			boolean validFields = true;
			TextField[] fields = {daysOnTripTextField, airfareTextField, carRentalTextField, milesDrivenTextField, parkingTextField, taxiChargesTextField, registrationTextField, lodgingTextField};
			double fieldValues[] = new double[8]; 
			//days, airfare, carRental, privateMilesDriven, parking, taxi, registration, lodging;
			
			for(int i = 0; i < fields.length; ++i) {
				try {
					fieldValues[i] = Double.parseDouble(fields[i].getText());
					
					if(fieldValues[i] < 0)
						throw new NumberFormatException();
					
					fields[i].setStyle(null);
				}
				catch(NumberFormatException e) {
					fields[i].setStyle("-fx-control-inner-background: rgb(255, 204, 203);");
					validFields = false;
				}
			}
			
			if(validFields) {
				DecimalFormat formatter = new DecimalFormat("$0.00");
				
				//Total variables
				double totalExpenses, totalAllowedExpenses, excess, saved;
				//Total Expenses Incurred
				totalExpenses = fieldValues[1] + fieldValues[2] + fieldValues[4] + fieldValues[5] + fieldValues[6] + (fieldValues[7] * fieldValues[0]);
				
				//Total Allowed Expenses
				totalAllowedExpenses = ((47 + 20 + 40 + 195) * fieldValues[0]) + (fieldValues[3] * 0.42);
				
				resultsLabel.setText("Total Allowed Expenses: " + formatter.format(totalAllowedExpenses) + "\r\n" +
									 "Total Expenses: " + formatter.format(totalExpenses) + "\r\n");
				
				if(totalExpenses > totalAllowedExpenses) {
					excess = totalExpenses - totalAllowedExpenses; //Excess to be paid
					resultsLabel.setText(resultsLabel.getText() + "Excess to be paid: " + formatter.format(excess));
				}
				else {
					saved = totalAllowedExpenses - totalExpenses;//Amount saved
					resultsLabel.setText(resultsLabel.getText() + "Amount Saved: " + formatter.format(saved));
				}
				
			}
			else
				resultsLabel.setText("\r\n\r\n");
		});
		
		//Work with VBox
		VBox labelsVBox = new VBox(15, daysOnTripLabel, airfareLabel, carRentalLabel, milesDrivenLabel, parkingLabel, taxiChargesLabel, registrationLabel, lodgingLabel);
		VBox fieldsVBox = new VBox(6, daysOnTripTextField, airfareTextField, carRentalTextField, milesDrivenTextField, parkingTextField, taxiChargesTextField, registrationTextField, lodgingTextField);
		VBox resultsVBox = new VBox(10, calculateExpensesButton, resultsLabel);
		
		labelsVBox.setAlignment(Pos.TOP_RIGHT);
		fieldsVBox.setAlignment(Pos.TOP_LEFT);
		resultsVBox.setAlignment(Pos.TOP_CENTER);
		
		//Work with BorderPane's
		BorderPane borderpane = new BorderPane(labelsVBox, null, fieldsVBox, resultsVBox, null);

		//Finish Fixing Margins on the results section
		BorderPane.setMargin(labelsVBox, new Insets(30, 0, 0, 0));
		BorderPane.setMargin(fieldsVBox, new Insets(30, 80, 0, 0));
		BorderPane.setMargin(resultsVBox, new Insets(0, 0, 30, 0));
		
		//Work with Scene
		Scene scene = new Scene(borderpane, 500, 420);
		
		//Work with Stage
		mainStage.setTitle("Travel Expenses");
		
		mainStage.setScene(scene);
		
		// Show the window.
		mainStage.show();

	}

}
