package Ch11_Lab08;

import java.text.DecimalFormat;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.CheckBox;
import javafx.geometry.Insets;
import javafx.geometry.Pos;

public class Ch11_Lab08 extends Application{

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage mainStage) throws Exception {
		//Work with Labels
		CheckBox oilCheck = new CheckBox("Oil change: $35.00");
		CheckBox lubeCheck = new CheckBox("Lube job: $25.00");
		CheckBox radiatorCheck = new CheckBox("Radiator flush: $50.00");
		CheckBox transmissionCheck = new CheckBox("Transmission flush: $120.00");
		CheckBox inspectionCheck = new CheckBox("Inspection: $35.00");
		CheckBox mufflerCheck = new CheckBox("Muffler replacement: $200.00");
		CheckBox tireCheck = new CheckBox("Tire rotation: $20.00");
		Label otherLabel = new Label("Hours worked on other services:  ");
		Label resultsLabel = new Label("");
		
		//Work with TextField
		TextField otherTextField = new TextField("");
		
		//Work with Buttons
		Button calculateMaintenanceButton = new Button("Calculate Maintenance");
		calculateMaintenanceButton.setOnAction(event -> {
			double totalCost = 0;
			boolean validFields = true;
			
			//Other Box Calculations Here
			try {
				double hours = Double.parseDouble(otherTextField.getText());
				
				if(hours < 0)
					throw new NumberFormatException();
				
				totalCost += hours * 60;
				otherTextField.setStyle(null);
			}
			catch(NumberFormatException e) {
				if(otherTextField.getText() != null && !otherTextField.getText().equals("")) {
					otherTextField.setStyle("-fx-control-inner-background: rgb(255, 204, 203);");
					validFields = false;
				}
			}
			
			if(validFields) {
				CheckBox[] checks = {oilCheck, lubeCheck, radiatorCheck, transmissionCheck, inspectionCheck, mufflerCheck, tireCheck};
				double[] prices = {35, 25, 50, 120, 35, 200, 20};
				
				for(int i = 0; i < checks.length; ++i) {
					if(checks[i].isSelected())
						totalCost += prices[i];
				}
				
				resultsLabel.setText("Total Maintenance Cost: " + (new DecimalFormat("$0.00").format(totalCost)));
			}
			else
				resultsLabel.setText("");
		});
		
		//Work with HBox
		HBox hbox = new HBox(otherLabel, otherTextField);
		hbox.setAlignment(Pos.CENTER);
		
		//Work with VBox's
		VBox checksVBox = new VBox(10, oilCheck, lubeCheck, radiatorCheck, transmissionCheck, inspectionCheck, mufflerCheck, tireCheck);
		VBox vbox = new VBox(10, checksVBox, hbox, calculateMaintenanceButton, resultsLabel);
		
		checksVBox.setAlignment(Pos.CENTER_LEFT);
		vbox.setAlignment(Pos.CENTER);
		VBox.setMargin(checksVBox, new Insets(0, 0, 0, 170));
		
		//Work with Scene
		Scene scene = new Scene(vbox, 500, 370);
		
		//Work with Stage
		mainStage.setTitle("Joe’s Automotive");
		
		mainStage.setScene(scene);
		
		// Show the window.
		mainStage.show();
	}

}