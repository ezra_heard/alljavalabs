package Ch11_Lab09;

import java.util.Random;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.geometry.Insets;
import javafx.geometry.Pos;

public class Ch11_Lab09 extends Application{

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage mainStage) throws Exception {
		//Work with Images
		ImageView square1 = new ImageView(new Image("file:src/Ch11_Lab09/blank.png"));
		ImageView square2 = new ImageView(new Image("file:src/Ch11_Lab09/blank.png"));
		ImageView square3 = new ImageView(new Image("file:src/Ch11_Lab09/blank.png"));
		ImageView square4 = new ImageView(new Image("file:src/Ch11_Lab09/blank.png"));
		ImageView square5 = new ImageView(new Image("file:src/Ch11_Lab09/blank.png"));
		ImageView square6 = new ImageView(new Image("file:src/Ch11_Lab09/blank.png"));
		ImageView square7 = new ImageView(new Image("file:src/Ch11_Lab09/blank.png"));
		ImageView square8 = new ImageView(new Image("file:src/Ch11_Lab09/blank.png"));
		ImageView square9 = new ImageView(new Image("file:src/Ch11_Lab09/blank.png"));
		
		//Work with Labels
		Label resultsLabel = new Label("");
		
		//Work with Buttons
		Button playButton = new Button("Play");
		playButton.setOnAction( e -> {
			boolean xWin = false, oWin = false;
			
			int[][] board = new int[3][3];
			ImageView[][] squareImages = {{square1, square2, square3}, {square4, square5, square6}, {square7, square8, square9}};
			
			Random rand = new Random();
			
			//0 = O && 1 = X
			//Fill the game board with random numbers between 0 and 1
			for(int i = 0; i < board.length; ++i) {
				for(int x = 0; x < board[i].length; ++x) {
					board[i][x] = rand.nextInt(2);
					
					//Display Images in Grid
					squareImages[i][x].setImage(new Image("file:src/Ch11_Lab09/" + (board[i][x] == 0 ? "O.png" : "X.png")));
				}
			}
			
			//Check Horizontal and Vertical
			for(int i = 0; i < 3; ++i) {
				if(board[i][0] == board[i][1] && board[i][1] == board[i][2]) {
					if(board[i][0] == 0)
						oWin = true;
					else if(board[i][0] == 1)
						xWin = true;
				}
				
				if(board[0][i] == board[1][i] && board[1][i] == board[2][i]) {
					if(board[0][i] == 0)
						oWin = true;
					else if(board[0][i] == 1)
						xWin = true;
				}
			}
			
			//Check Diagonals
			if(board[0][0] == board[1][1] && board[1][1] == board[2][2]) {
				if(board[0][0] == 0)
					oWin = true;
				else if(board[0][0] == 1)
					xWin = true;
			}
			if(board[0][2] == board[1][1] && board[1][1] == board[2][0]) {
				if(board[0][2] == 0)
					oWin = true;
				else if(board[0][2] == 1)
					xWin = true;
			}
			
			//Display Results
			if(xWin == oWin)
				resultsLabel.setText("It's a Tie!");
			else if(xWin)
				resultsLabel.setText("X Wins!");
			else if(oWin)
				resultsLabel.setText("O Wins!");
		});
		
		//Work with GridPane
		GridPane squares = new GridPane();
		squares.setStyle("-fx-border-color: black; -fx-border-width: 5px; -fx-background-color: grey;");
		squares.setAlignment(Pos.CENTER);
		
		squares.add(square1, 0, 0);
		squares.add(square2, 1, 0);
		squares.add(square3, 2, 0);
		squares.add(square4, 0, 1);
		squares.add(square5, 1, 1);
		squares.add(square6, 2, 1);
		squares.add(square7, 0, 2);
		squares.add(square8, 1, 2);
		squares.add(square9, 2, 2);
		
		squares.setVgap(5);
		squares.setHgap(5);
		
		//Work with VBox
		VBox vbox = new VBox(10, resultsLabel, playButton);
		vbox.setAlignment(Pos.CENTER);
		vbox.setPadding(new Insets(10, 0, 0, 0));
		
		//Work with BorderPane
		BorderPane borderPane = new BorderPane(squares, null, null, vbox, null);
		borderPane.setPadding(new Insets(20));
		
		//Work with Scene
		Scene scene = new Scene(borderPane, 250, 300);
		
		//Work with Stage
		mainStage.setTitle("Tic-Tac-Toe");
		
		mainStage.setScene(scene);
		
		// Show the window.
		mainStage.show();
	}

}
