package Ch11_Lab10;

import java.util.Random;
import java.text.DecimalFormat;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;
import javafx.geometry.Insets;
import javafx.geometry.Pos;

public class Ch11_Lab10 extends Application{

	private double winnings;
	
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage mainStage) throws Exception {
		winnings = 0;
		
		//Work with Labels
		Label betLabel = new Label("Amount Inserted: $");
		Label amountWonLabel = new Label("Amount Won This Spin: $0.00");
		Label totalWonLabel = new Label("Total Amount Won: $0.00");
		
		//Work with TextFields
		TextField betTextField = new TextField("0.00");
		
		//Work with Image Views
		ImageView slot1ImageView = new ImageView(new Image("file:src/Ch11_Lab10/cherries.jpg"));
		ImageView slot2ImageView = new ImageView(new Image("file:src/Ch11_Lab10/cherries.jpg"));
		ImageView slot3ImageView = new ImageView(new Image("file:src/Ch11_Lab10/cherries.jpg"));
		
//		slot1ImageView.setStyle("-fx-border-color: black; -fx-border-width: 3px;");
//		slot2ImageView.setStyle("-fx-border-color: black; -fx-border-width: 3px;");
//		slot3ImageView.setStyle("-fx-border-color: black; -fx-border-width: 3px;");
		
		//Work with Buttons
		Button spinButton = new Button("Spin");
		spinButton.setOnAction(event -> {
			boolean validFields = true;
			double bet = 0;
			
			try {
				bet = Double.parseDouble(betTextField.getText());
			}
			catch(NumberFormatException e) {
				validFields = false;
				amountWonLabel.setText("Amount Won This Spin: $0.00");
			}
			
			if(validFields) {
				int slot1 = -1, slot2 = -1, slot3 = -1;
				double amountWon = 0;
				
				DecimalFormat formatter = new DecimalFormat("$0.00");
				Random rand = new Random();
				
				String fileString = "file:src/Ch11_Lab10/";
				Image[] slotPhotos = {new Image(fileString + "cherries.jpg"), new Image(fileString + "apple.png"),
									  new Image(fileString + "grapes.jpg"), new Image(fileString + "orange.png"),
									  new Image(fileString + "lemon.jpg")};
				
				slot1 = rand.nextInt(5);
				slot2 = rand.nextInt(5);
				slot3 = rand.nextInt(5);
				
				//Update Slot Displays
				slot1ImageView.setImage(slotPhotos[slot1]);
				slot2ImageView.setImage(slotPhotos[slot2]);
				slot3ImageView.setImage(slotPhotos[slot3]);
				
				//Calculations
				if(slot1 == slot2 && slot2 == slot3) {
					amountWon = bet * 3;
					winnings += amountWon;
				}
				else if(slot1 == slot2 || slot1 == slot3 || slot2 == slot3) {
					amountWon = bet * 2;
					winnings += amountWon;
				}
				
				//Display Results
				amountWonLabel.setText("Amount Won This Spin: " + formatter.format(amountWon));
				totalWonLabel.setText("Total Amount Won: " + formatter.format(winnings));
			}
			
		});
		
		//Work with HBox
		HBox slot1HBox = new HBox(slot1ImageView);
		HBox slot2HBox = new HBox(slot2ImageView);
		HBox slot3HBox = new HBox(slot3ImageView);
		HBox slotsHBox = new HBox(15, slot1HBox, slot2HBox, slot3HBox);
		HBox betHBox = new HBox(betLabel, betTextField);
		
		betHBox.setPadding(new Insets(15, 0, 0 ,0));
		
		slot1HBox.setStyle("-fx-border-color: black; -fx-border-width: 5px;");
		slot2HBox.setStyle("-fx-border-color: black; -fx-border-width: 5px;");
		slot3HBox.setStyle("-fx-border-color: black; -fx-border-width: 5px;");
		
		//Work with VBox
		VBox resultsVBox = new VBox(10, amountWonLabel, totalWonLabel);
		resultsVBox.setPadding(new Insets(15, 0, 0 ,0));
		
		//Work with BorderPane's
		BorderPane borderpane = new BorderPane(null, slotsHBox, resultsVBox, spinButton, betHBox);

		//BorderPane.setAlignment(slotsHBox, Pos.CENTER);
		BorderPane.setAlignment(spinButton, Pos.BOTTOM_CENTER);
		
		BorderPane.setMargin(slotsHBox, new Insets(0, 0, 0, 20));
		borderpane.setPadding(new Insets(50));
		//BorderPane.setMargin(resultsVBox, new Insets(0, 0, 50, 0));
		
		//Work with Scene
		Scene scene = new Scene(borderpane, 800, 370);
		
		//Work with Stage
		mainStage.setTitle("Slot Machine");
		
		mainStage.setScene(scene);
		
		// Show the window.
		mainStage.show();
	}
}
