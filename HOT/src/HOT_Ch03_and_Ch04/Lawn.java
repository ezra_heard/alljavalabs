package HOT_Ch03_and_Ch04;

public class Lawn {
	//Instance fields
	private int width;
	private int length;
	
	//Constructors
	public Lawn() {}
	
	public Lawn(int width, int length) {
		this.width = width;
		this.length = length;
	}

	//Accessors and Mutators
	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}
	
	//Methods
	public int calcSquareFeet() {
		return width * length;
	}
	
}
