package HOT_Ch03_and_Ch04;

import java.util.Scanner;

public class LawnDriver {

	public static void main(String[] args) {
		Lawn lawn1, lawn2 = new Lawn();
		int lawn1Width;
		
		Scanner userInput = new Scanner(System.in);
		
		//Get lawn 1 info
		System.out.print("Enter Lawn 1's width: ");
		lawn1Width = userInput.nextInt();
		
		System.out.print("Enter Lawn 1's length: ");
		
		//Generate lawn1
		lawn1 = new Lawn(lawn1Width, userInput.nextInt());
		
		//Get lawn 2 info
		System.out.print("Enter Lawn 2's width: ");
		lawn2.setWidth(userInput.nextInt());
		
		System.out.print("Enter Lawn 2's length: ");
		lawn2.setLength(userInput.nextInt());
		
		System.out.println();
		
		//Print weekly fee for lawn 1
		System.out.printf("Lawn 1's weekly fee: $%.2f\n", (double)calcWeeklyFee(lawn1.calcSquareFeet()));
		//Print 20-week fee for lawn 1
		System.out.printf("Lawn 1's 20-week fee: $%.2f\n", (double)(calcWeeklyFee(lawn1.calcSquareFeet()) * 20));
		
		System.out.println();
		
		//Print weekly fee for lawn 2
		System.out.printf("Lawn 2's weekly fee: $%.2f\n", (double)calcWeeklyFee(lawn2.calcSquareFeet()));
		//Print 20-week fee for lawn 2
		System.out.printf("Lawn 2's 20-week fee: $%.2f\n", (double)(calcWeeklyFee(lawn2.calcSquareFeet()) * 20));
		
		userInput.close();
	}

	private static int calcWeeklyFee(int size) {
		if(size < 400)
			return 25;
		else if(size >= 400 && size < 600)
			return 35;
		else
			return 50;
	}
}
