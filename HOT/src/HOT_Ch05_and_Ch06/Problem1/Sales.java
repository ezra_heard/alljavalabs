package HOT_Ch05_and_Ch06.Problem1;

import java.util.Scanner;
import java.io.*;

public class Sales {
	public static void main(String[] args) throws IOException {
		double totalSales = 0;
		
		Scanner userInput = new Scanner(System.in);
		PrintWriter pw = new PrintWriter("src/HOT_Ch05_and_Ch06/Problem1/WeeklySales.txt");
		
		for(int i = 1; i <= 5; ++i) {
			
			double sale = 0;
			
			do {
				System.out.print("Enter day " + i + "'s sale: ");
				sale = userInput.nextDouble();
				
				if(sale < 0)
					System.out.println("Error: sale can't be negative");
			}while(sale < 0);
			
			pw.println("Day " + i + ": $" + String.format("%,.2f", sale));
			
			totalSales += sale;
		}
		
		pw.println();
		pw.println("Total Sales: $" + String.format("%,.2f", totalSales));
		
		userInput.close();
		pw.close();
		 
		System.out.println("Sales file successfully created");
	}
}
