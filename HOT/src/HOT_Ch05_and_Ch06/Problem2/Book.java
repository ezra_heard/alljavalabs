package HOT_Ch05_and_Ch06.Problem2;

public class Book {
	private String name;
	private String publishDate;
	private String author;
	
	public Book(String name, String publishDate, String author) {
		this.name = name;
		this.publishDate = publishDate;
		this.author = author;
	}

	public String toString() {
		return "Book Name: " + this.name + "\nPublication Date: " + this.publishDate + "\nAuthor: " + this.author;
	}
}
