package HOT_Ch05_and_Ch06.Problem2;

public class Library {
	private Book book;
	private String name;
	
	public Library(Book book) {
		this.book = book; //security hole
		this.name = "";
	}
	
	public Library(Book book, String name) {
		this.book = book;
		this.name = name;
	}
	
	public String toString() {
		return "Library: " + this.name + "\n" + book.toString();
	}
}
