package HOT_Ch05_and_Ch06.Problem2;

public class LibraryDriver {

	public static void main(String[] args) {
		Library library = new Library(new Book("The Shape of Illusion", "12/12/2012", "Some Dude"), "My Library");
		
		System.out.println(library.toString());
	}

}
