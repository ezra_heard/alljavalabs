package HOT_Ch05_and_Ch06.Problem3;

public class Hot3Driver {
	public static void main(String[] args) {
		System.out.println("25 + 999 = " + AddSomeNumbers.addNumbers(25, 999));
		System.out.println("0.001 + 0.251 + 1.734 = " + AddSomeNumbers.addNumbers(0.001, 0.251, 1.734));
	}
}
