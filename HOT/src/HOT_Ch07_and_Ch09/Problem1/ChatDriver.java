package HOT_Ch07_and_Ch09.Problem1;

import java.util.Scanner;

public class ChatDriver {

	public static void main(String[] args) {
		int userCode; //Holds the area code the user enters
		double callLength; //Holds the call length the user enters
		double cost; //Holds the total cost returned from the calcCallPrice method
		
		//Preassigned parallel array that hold valid area codes and per-minute rates
		int[] areaCodes = {262, 414, 608, 715, 815, 920};
		double[] rates = {0.07, 0.10, 0.05, 0.16, 0.24, 0.14};
		
		Scanner userInput = new Scanner(System.in); //Keyboard scanner to obtain user input
		
		//Ask user for call length and store it in the callLength variable
		System.out.print("Enter call length: ");
		callLength = userInput.nextDouble();
		
		//Ask user for area code and store it in the userCode variable
		System.out.print("Enter area code: ");
		userCode = userInput.nextInt();
		
		//Pass user input and parallel arrays to method in order to calculate price
		cost = calcCallPrice(userCode, callLength, areaCodes, rates);
		
		//If the area code was valid display the cost, else display an error message
		if(cost != -1)
			System.out.printf("That call costs: $%,.2f", cost);
		else
			System.out.println("Invlaid area code, please try again");
		
		//Close stream
		userInput.close();
	}

	//Calculates to calls price based on the area code's rate and the call length
	private static double calcCallPrice(int code, double callLength, int[] codeList, double[] priceList) {
		
		//Sequential Search to find the area codes index
		for(int i = 0; i < codeList.length; ++i) {
			//When the area code is found return the price for that code times the calls length
			if(code == codeList[i])
				return priceList[i] * callLength;
		}
	
		//if the code is not found in the array returns -1 to show the area code is invalid
		return -1;
	}
}
