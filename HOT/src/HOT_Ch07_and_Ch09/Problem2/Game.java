package HOT_Ch07_and_Ch09.Problem2;

public class Game {
	private String name; //Holds the name of the game
	private int maxPlayers; //Holds the maximum number of players that can play the game
	
	//Getters and Setters for the private fields
	public String getName() { return name; }
	public int getMaxPlayers() { return maxPlayers; }
	
	public void setName(String name) { this.name = name; }
	public void setMaxPlayers(int maxPlayers) { this.maxPlayers = maxPlayers; }
	
	//Override the toString method to return all the info about this instance of the Game object
	@Override
	public String toString() {
		return "Game: " + this.name + "\nMax Players: " + maxPlayers;
	}
}
