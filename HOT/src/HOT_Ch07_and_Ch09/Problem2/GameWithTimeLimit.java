package HOT_Ch07_and_Ch09.Problem2;

public class GameWithTimeLimit extends Game{
	private int timeLimit; //Holds the time limit of the game

	//Getters and Setters for the time limit
	public int getTimeLimit() { return timeLimit; }
	public void setTimeLimit(int timeLimit) { this.timeLimit = timeLimit; }
	
	//Override the toString method to return superclass's toString and this instance of the objects time limit
	@Override
	public String toString() {
		return super.toString() + "\nTime Limit: " + timeLimit + " minutes"; 
	}
}
