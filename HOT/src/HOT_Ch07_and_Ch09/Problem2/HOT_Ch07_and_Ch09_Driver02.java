package HOT_Ch07_and_Ch09.Problem2;

public class HOT_Ch07_and_Ch09_Driver02 {
	public static void main(String[] args) {
		
		Game videoGame = new Game(); //Creates a new Game object
		GameWithTimeLimit boardGame = new GameWithTimeLimit(); //Creates a new GameWithTimeLimit object
		
		//Sets the fields for the videoGame object
		videoGame.setName("Skyrim");
		videoGame.setMaxPlayers(1);
		
		//Sets the fields for the boardGame object
		boardGame.setName("Big Boggle");
		boardGame.setMaxPlayers(4);
		boardGame.setTimeLimit(30);
		
		//Display the videoGame Object and the boardGame object to the console
		System.out.println(videoGame);
		System.out.println();
		System.out.println(boardGame);
	}
}
