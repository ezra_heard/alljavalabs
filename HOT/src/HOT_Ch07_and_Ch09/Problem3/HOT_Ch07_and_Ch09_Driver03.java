package HOT_Ch07_and_Ch09.Problem3;

public class HOT_Ch07_and_Ch09_Driver03 {

	public static void main(String[] args) {
		Vehicle car = new Car();
		Vehicle truck = new Truck();
		
		System.out.println("Car Speed: " + car.getSpeed());
		System.out.println("Truck Speed: " + truck.getSpeed());
		
		car.accelerate();
		truck.accelerate();
		
		System.out.println("Car Speed after accel: " + car.getSpeed());
		System.out.println("Truck Speed afet accel: " + truck.getSpeed());
	}

}
