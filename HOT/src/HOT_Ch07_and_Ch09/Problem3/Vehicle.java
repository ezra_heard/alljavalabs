package HOT_Ch07_and_Ch09.Problem3;

public class Vehicle {
	protected int speed = 0;
	
	public void accelerate() {
		speed += 5;
	}
	
	public int getSpeed() { return speed; }
}
