package HOT_Ch10_and_Ch11;

import java.text.DecimalFormat;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.CheckBox;
import javafx.geometry.Insets;
import javafx.geometry.Pos;

public class HOT_Ch10_and_Ch11 extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage mainStage) throws Exception {
		//Work with CheckBoxs
		CheckBox oilCheck = new CheckBox("Oil Change ($26.00)");
		CheckBox lubeCheck = new CheckBox("Lube Job ($18.00)");
		CheckBox radiatorCheck = new CheckBox("Radiator Flush ($30.00)");
		CheckBox transmissionCheck = new CheckBox("Transmission Flush ($80.00)");
		CheckBox inspectionCheck = new CheckBox("Inspection ($15.00)");
		CheckBox mufflerCheck = new CheckBox("Muffler Replacement ($100.00)");
		CheckBox tireCheck = new CheckBox("Tire Rotation ($20.00)");
		
		//Work with Labels
		Label routineServicesLabel = new Label("Routine Services");
		Label nonroutineServicesLabel = new Label("Nonroutine Services");
		Label partsChargeLabel = new Label("Parts Charge: ");
		Label laborHoursLabel = new Label("Hours of Labor: ");
		Label resultsLabel = new Label("Total Charges: ");
		
		routineServicesLabel.setStyle("-fx-font-weight: bold;");
		nonroutineServicesLabel.setStyle("-fx-font-weight: bold;");
		
		//Work with TextField
		TextField partsChargeTextField = new TextField("");
		TextField laborHoursTextField = new TextField("");
		
		//Work with Buttons
		Button exitButton = new Button("Exit");
		exitButton.setPadding(new Insets(2, 20, 2, 20));
		exitButton.setOnAction(event -> {
			System.exit(0);
		});
		
		Button calculateChargesButton = new Button("Calculate Charges");
		calculateChargesButton.setPadding(new Insets(2, 30, 2, 30));
		calculateChargesButton.setOnAction(event -> {
			double totalCost = 0;
			boolean validFields = true;
			
			//Try Parts Charge TextField
			try {
				double partsCharge = Double.parseDouble(partsChargeTextField.getText());
				
				if(partsCharge < 0)
					throw new NumberFormatException();
				
				totalCost += partsCharge;
				
				partsChargeTextField.setStyle(null);
			}
			catch(NumberFormatException e) {
				if(partsChargeTextField.getText() != null && !partsChargeTextField.getText().isEmpty()) {
					partsChargeTextField.setStyle("-fx-control-inner-background: rgb(255, 204, 203);");
					validFields = false;
				}
				else
					partsChargeTextField.setStyle(null);
			}
			
			//Try Hours of Labor TextField
			try{
				double hours = Double.parseDouble(laborHoursTextField.getText());
				
				if(hours < 0)
					throw new NumberFormatException();
				
				totalCost += hours * 20;
				
				laborHoursTextField.setStyle(null);
			}
			catch(NumberFormatException e) {
				if(laborHoursTextField.getText() != null && !laborHoursTextField.getText().isEmpty()) {
					laborHoursTextField.setStyle("-fx-control-inner-background: rgb(255, 204, 203);");
					validFields = false;
				}
				else
					laborHoursTextField.setStyle(null);
			}
			
			if(validFields) {
				CheckBox[] checks = {oilCheck, lubeCheck, radiatorCheck, transmissionCheck, inspectionCheck, mufflerCheck, tireCheck};
				double[] prices = {26, 18, 30, 80, 15, 100, 20};
				
				for(int i = 0; i < checks.length; ++i) {
					if(checks[i].isSelected())
						totalCost += prices[i];
				}
				
				resultsLabel.setText("Total Charges: " + (new DecimalFormat("$0.00").format(totalCost)));
			}
			else
				resultsLabel.setText("Total Charges: ");
		});
		
		//Work with HBoxes
		HBox partsChargeHBox = new HBox(36, partsChargeLabel, partsChargeTextField);
		HBox laborHoursHBox = new HBox(25, laborHoursLabel, laborHoursTextField);
		HBox buttonsHBox = new HBox(10, calculateChargesButton, exitButton);
		buttonsHBox.setAlignment(Pos.CENTER);
		
		//Work with VBoxes
		VBox checksVBox = new VBox(10, oilCheck, lubeCheck, radiatorCheck, transmissionCheck, inspectionCheck, mufflerCheck, tireCheck);
		VBox vbox = new VBox(5, routineServicesLabel, checksVBox, nonroutineServicesLabel, partsChargeHBox, laborHoursHBox, buttonsHBox, resultsLabel);
		vbox.setPadding(new Insets(20));
		VBox.setMargin(resultsLabel, new Insets(10, 0, 0, 0));
		
		checksVBox.setAlignment(Pos.CENTER_LEFT);
		vbox.setAlignment(Pos.CENTER_LEFT);
		
		//Work with Scene
		Scene scene = new Scene(vbox, 300, 350);
		
		//Work with Stage
		mainStage.setTitle("Ranken's Automotive Maintenance");
		
		mainStage.setScene(scene);
		
		// Show the window.
		mainStage.show();
	}

}
