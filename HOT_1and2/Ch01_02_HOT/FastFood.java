import java.util.Scanner;

public class FastFood{
	public static void main(String[] args){
		/////////////////////
		//Declare Variables//
		/////////////////////
		//Create constants for each of the food prices
		final double HAMBURGER_PRICE = 1.25;
		final double CHEESEBURGER_PRICE = 1.50;
		final double SODA_PRICE = 1.95;
		final double FRIES_PRICE = 0.95;
		
		//Create an integer for the number of each of the foods ordered
		int numberOfHambergers, numberOfCheeseburgers, numberOfSodas, numberOfFries;
		
		//Create an double for the total price of each of the foods ordered and the total overall price of the order
		double total, priceBurgers, priceCheeseburgers, priceSodas, priceFries;
		
		//Create a String that holds the users name
		String name;
		
		//Create Scanner object that receives input from keyboard
		Scanner userInput = new Scanner(System.in);
		
		//////////////////
		//Get user input//
		//////////////////
		//Ask for number of hamburgers and store in numberOfHambergers variable
		System.out.print("How many Hamburgers? ");
		numberOfHambergers = userInput.nextInt();
		
		//Ask for number of cheeseburgers and store in numberOfCheeseburgers variable
		System.out.print("How many Cheeseburgers? ");
		numberOfCheeseburgers = userInput.nextInt();
		
		//Ask for number of sodas and store in numberOfSodas variable
		System.out.print("How many Sodas? ");
		numberOfSodas = userInput.nextInt();
		
		//Ask for number of fries and store in numberOfFries variable
		System.out.print("How many orders of Fries? ");
		numberOfFries = userInput.nextInt();
		
		//Clear keyboard buffer
		userInput.nextLine();
		
		//Ask for name and store in name variable
		System.out.print("What is your Name? ");
		name = userInput.nextLine();
		
		/////////////////////////////
		//Mathematical Calculations//
		/////////////////////////////
		priceBurgers = numberOfHambergers * HAMBURGER_PRICE; 				//Calculate the price for the hamburgers ordered
		priceCheeseburgers = numberOfCheeseburgers * CHEESEBURGER_PRICE;	//Calculate the price for the cheeseburgers ordered
		priceSodas = numberOfSodas * SODA_PRICE;							//Calculate the price for the sodas ordered
		priceFries = numberOfFries * FRIES_PRICE;							//Calculate the price for the fries ordered
		
		//Calculate the total price based on the total price of each of the food totals
		total = priceBurgers + priceCheeseburgers + priceSodas + priceFries;
		
		///////////////////
		//Display Results//
		///////////////////
		System.out.println(); //Add an extra space before the results
		
		//Display the users name in all caps
		System.out.println(name.toUpperCase());
		
		//Display the total formated with commas to two decimal points of precision
		System.out.printf("Total: $%,.2f", total);
	}
}